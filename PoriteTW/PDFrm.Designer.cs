﻿namespace PoriteTW
{
    partial class PDFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PDFrm));
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.dtpPD = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.tbxExit = new System.Windows.Forms.Button();
            this.tbxPerson = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pnlScan = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbxDate = new System.Windows.Forms.TextBox();
            this.lblPDArea = new System.Windows.Forms.Label();
            this.lblPerson = new System.Windows.Forms.Label();
            this.tbxPDNum = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            this.tbxHint = new System.Windows.Forms.TextBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.tbxUnit = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbxLOTA = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbxQTY = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxGLOTA = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbxMATERIAL = new System.Windows.Forms.TextBox();
            this.tbx2D = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlMain.SuspendLayout();
            this.pnlScan.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.pictureBox3);
            this.pnlMain.Controls.Add(this.dtpPD);
            this.pnlMain.Controls.Add(this.label16);
            this.pnlMain.Controls.Add(this.comboBox1);
            this.pnlMain.Controls.Add(this.label15);
            this.pnlMain.Controls.Add(this.label12);
            this.pnlMain.Controls.Add(this.label13);
            this.pnlMain.Controls.Add(this.btnNext);
            this.pnlMain.Controls.Add(this.tbxExit);
            this.pnlMain.Controls.Add(this.tbxPerson);
            this.pnlMain.Controls.Add(this.label9);
            this.pnlMain.Location = new System.Drawing.Point(3, 3);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(254, 319);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(4, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(94, 43);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // dtpPD
            // 
            this.dtpPD.Location = new System.Drawing.Point(93, 118);
            this.dtpPD.Name = "dtpPD";
            this.dtpPD.Size = new System.Drawing.Size(138, 22);
            this.dtpPD.TabIndex = 75;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(7, 122);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(88, 16);
            this.label16.Text = "盤點日期：";
            // 
            // comboBox1
            // 
            this.comboBox1.Items.Add("资材");
            this.comboBox1.Items.Add("仓库");
            this.comboBox1.Items.Add("现场");
            this.comboBox1.Location = new System.Drawing.Point(93, 90);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(138, 22);
            this.comboBox1.TabIndex = 65;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(5, 93);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(88, 16);
            this.label15.Text = "盤點區域：";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(104, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(135, 45);
            this.label12.Text = "盤點操作 - \r\n    掃描員工號、選擇盤點區域進行盤點操作";
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.SteelBlue;
            this.label13.Location = new System.Drawing.Point(2, 50);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(239, 4);
            // 
            // btnNext
            // 
            this.btnNext.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.btnNext.Location = new System.Drawing.Point(130, 284);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(101, 25);
            this.btnNext.TabIndex = 59;
            this.btnNext.Text = "下一步>>";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // tbxExit
            // 
            this.tbxExit.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.tbxExit.Location = new System.Drawing.Point(7, 284);
            this.tbxExit.Name = "tbxExit";
            this.tbxExit.Size = new System.Drawing.Size(101, 25);
            this.tbxExit.TabIndex = 58;
            this.tbxExit.Text = "退出操作";
            this.tbxExit.Click += new System.EventHandler(this.tbxExit_Click);
            // 
            // tbxPerson
            // 
            this.tbxPerson.Location = new System.Drawing.Point(93, 64);
            this.tbxPerson.Name = "tbxPerson";
            this.tbxPerson.Size = new System.Drawing.Size(138, 21);
            this.tbxPerson.TabIndex = 49;
            this.tbxPerson.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxPerson_KeyPress);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(7, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 20);
            this.label9.Text = "掃描員工號：";
            // 
            // pnlScan
            // 
            this.pnlScan.Controls.Add(this.btnSave);
            this.pnlScan.Controls.Add(this.tbxDate);
            this.pnlScan.Controls.Add(this.lblPDArea);
            this.pnlScan.Controls.Add(this.lblPerson);
            this.pnlScan.Controls.Add(this.tbxPDNum);
            this.pnlScan.Controls.Add(this.label8);
            this.pnlScan.Controls.Add(this.btnExport);
            this.pnlScan.Controls.Add(this.tbxHint);
            this.pnlScan.Controls.Add(this.btnBack);
            this.pnlScan.Controls.Add(this.tbxUnit);
            this.pnlScan.Controls.Add(this.label7);
            this.pnlScan.Controls.Add(this.tbxLOTA);
            this.pnlScan.Controls.Add(this.label6);
            this.pnlScan.Controls.Add(this.tbxQTY);
            this.pnlScan.Controls.Add(this.label5);
            this.pnlScan.Controls.Add(this.tbxGLOTA);
            this.pnlScan.Controls.Add(this.label4);
            this.pnlScan.Controls.Add(this.tbxMATERIAL);
            this.pnlScan.Controls.Add(this.tbx2D);
            this.pnlScan.Controls.Add(this.label2);
            this.pnlScan.Controls.Add(this.label3);
            this.pnlScan.Controls.Add(this.label1);
            this.pnlScan.Location = new System.Drawing.Point(269, 3);
            this.pnlScan.Name = "pnlScan";
            this.pnlScan.Size = new System.Drawing.Size(244, 319);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(82, 284);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(72, 25);
            this.btnSave.TabIndex = 101;
            this.btnSave.Text = "確認盤點";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbxDate
            // 
            this.tbxDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tbxDate.Location = new System.Drawing.Point(97, 208);
            this.tbxDate.Name = "tbxDate";
            this.tbxDate.ReadOnly = true;
            this.tbxDate.Size = new System.Drawing.Size(138, 21);
            this.tbxDate.TabIndex = 90;
            // 
            // lblPDArea
            // 
            this.lblPDArea.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.lblPDArea.Location = new System.Drawing.Point(11, 34);
            this.lblPDArea.Name = "lblPDArea";
            this.lblPDArea.Size = new System.Drawing.Size(224, 18);
            this.lblPDArea.Text = "盤點區域：";
            // 
            // lblPerson
            // 
            this.lblPerson.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.lblPerson.Location = new System.Drawing.Point(11, 8);
            this.lblPerson.Name = "lblPerson";
            this.lblPerson.Size = new System.Drawing.Size(224, 24);
            this.lblPerson.Text = "掃描員工號：";
            // 
            // tbxPDNum
            // 
            this.tbxPDNum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tbxPDNum.Location = new System.Drawing.Point(97, 234);
            this.tbxPDNum.Name = "tbxPDNum";
            this.tbxPDNum.ReadOnly = true;
            this.tbxPDNum.Size = new System.Drawing.Size(138, 21);
            this.tbxPDNum.TabIndex = 78;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(11, 209);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 20);
            this.label8.Text = "日期：";
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(157, 284);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(72, 25);
            this.btnExport.TabIndex = 57;
            this.btnExport.Text = "保存數據";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // tbxHint
            // 
            this.tbxHint.BackColor = System.Drawing.Color.Red;
            this.tbxHint.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbxHint.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.tbxHint.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.tbxHint.Location = new System.Drawing.Point(7, 260);
            this.tbxHint.Multiline = true;
            this.tbxHint.Name = "tbxHint";
            this.tbxHint.ReadOnly = true;
            this.tbxHint.Size = new System.Drawing.Size(228, 22);
            this.tbxHint.TabIndex = 56;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(7, 284);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(72, 25);
            this.btnBack.TabIndex = 54;
            this.btnBack.Text = "<<上一步";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // tbxUnit
            // 
            this.tbxUnit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tbxUnit.Location = new System.Drawing.Point(97, 181);
            this.tbxUnit.Name = "tbxUnit";
            this.tbxUnit.ReadOnly = true;
            this.tbxUnit.Size = new System.Drawing.Size(138, 21);
            this.tbxUnit.TabIndex = 53;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(11, 184);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 20);
            this.label7.Text = "單位：";
            // 
            // tbxLOTA
            // 
            this.tbxLOTA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tbxLOTA.Location = new System.Drawing.Point(97, 156);
            this.tbxLOTA.Name = "tbxLOTA";
            this.tbxLOTA.ReadOnly = true;
            this.tbxLOTA.Size = new System.Drawing.Size(138, 21);
            this.tbxLOTA.TabIndex = 52;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(11, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 20);
            this.label6.Text = "條碼ID：";
            // 
            // tbxQTY
            // 
            this.tbxQTY.BackColor = System.Drawing.Color.White;
            this.tbxQTY.Location = new System.Drawing.Point(97, 131);
            this.tbxQTY.Name = "tbxQTY";
            this.tbxQTY.Size = new System.Drawing.Size(138, 21);
            this.tbxQTY.TabIndex = 51;
            this.tbxQTY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxQTY_KeyPress);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(11, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 20);
            this.label5.Text = "數量：";
            // 
            // tbxGLOTA
            // 
            this.tbxGLOTA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tbxGLOTA.Location = new System.Drawing.Point(97, 106);
            this.tbxGLOTA.Name = "tbxGLOTA";
            this.tbxGLOTA.ReadOnly = true;
            this.tbxGLOTA.Size = new System.Drawing.Size(138, 21);
            this.tbxGLOTA.TabIndex = 50;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(11, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 20);
            this.label4.Text = "批次：";
            // 
            // tbxMATERIAL
            // 
            this.tbxMATERIAL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tbxMATERIAL.Location = new System.Drawing.Point(97, 80);
            this.tbxMATERIAL.Name = "tbxMATERIAL";
            this.tbxMATERIAL.ReadOnly = true;
            this.tbxMATERIAL.Size = new System.Drawing.Size(138, 21);
            this.tbxMATERIAL.TabIndex = 49;
            // 
            // tbx2D
            // 
            this.tbx2D.Location = new System.Drawing.Point(97, 55);
            this.tbx2D.Name = "tbx2D";
            this.tbx2D.Size = new System.Drawing.Size(138, 21);
            this.tbx2D.TabIndex = 48;
            this.tbx2D.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbx2D_KeyPress);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(11, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 20);
            this.label2.Text = "掃描二維碼：";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(11, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 20);
            this.label3.Text = "物料編碼：";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(11, 235);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 20);
            this.label1.Text = "已盤總數：";
            // 
            // PDFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(608, 349);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlScan);
            this.MinimizeBox = false;
            this.Name = "PDFrm";
            this.Text = "保來得原油盤點";
            this.Load += new System.EventHandler(this.PDFrm_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.PDFrm_Closing);
            this.pnlMain.ResumeLayout(false);
            this.pnlScan.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.DateTimePicker dtpPD;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button tbxExit;
        private System.Windows.Forms.TextBox tbxPerson;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel pnlScan;
        private System.Windows.Forms.TextBox tbxPDNum;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.TextBox tbxHint;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.TextBox tbxUnit;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbxLOTA;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbxQTY;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbxGLOTA;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbxMATERIAL;
        private System.Windows.Forms.TextBox tbx2D;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblPDArea;
        private System.Windows.Forms.Label lblPerson;
        private System.Windows.Forms.TextBox tbxDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSave;
    }
}