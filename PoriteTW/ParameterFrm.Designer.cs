﻿namespace PoriteTW
{
    partial class ParameterFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ParameterFrm));
            System.Windows.Forms.ListViewItem listViewItem17 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem18 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem19 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem20 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem21 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem22 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem23 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem24 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem25 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem26 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem27 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem28 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem29 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem30 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem31 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem32 = new System.Windows.Forms.ListViewItem();
            this.inputPanel1 = new Microsoft.WindowsCE.Forms.InputPanel(this.components);
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tbxClient = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btnWebTest = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblIPAddress = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSQLSave = new System.Windows.Forms.Button();
            this.tbxPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbxUser = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxPort = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxServer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblHint = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.btnDeleteArea = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label15 = new System.Windows.Forms.Label();
            this.btnSaveArea = new System.Windows.Forms.Button();
            this.tbxPDCode = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbxPDArea = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnSaveCoord = new System.Windows.Forms.Button();
            this.tbxYCoord = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbxXCoord = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.label18 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnBTSave = new System.Windows.Forms.Button();
            this.tbxBTAddress = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btnInitPD = new System.Windows.Forms.Button();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tbxClient);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.btnWebTest);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.lblIPAddress);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.btnSQLSave);
            this.tabPage1.Controls.Add(this.tbxPassword);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.tbxUser);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.tbxPort);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.tbxServer);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(0, 0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(246, 296);
            this.tabPage1.Text = "服務器設定";
            // 
            // tbxClient
            // 
            this.tbxClient.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.tbxClient.Location = new System.Drawing.Point(95, 167);
            this.tbxClient.Name = "tbxClient";
            this.tbxClient.Size = new System.Drawing.Size(138, 22);
            this.tbxClient.TabIndex = 55;
            this.tbxClient.Text = "411";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label16.Location = new System.Drawing.Point(9, 171);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(88, 15);
            this.label16.Text = "Client No：";
            // 
            // btnWebTest
            // 
            this.btnWebTest.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.btnWebTest.Location = new System.Drawing.Point(9, 257);
            this.btnWebTest.Name = "btnWebTest";
            this.btnWebTest.Size = new System.Drawing.Size(101, 25);
            this.btnWebTest.TabIndex = 45;
            this.btnWebTest.Text = "WS測試";
            this.btnWebTest.Click += new System.EventHandler(this.btnWebTest_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(4, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(94, 43);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // lblIPAddress
            // 
            this.lblIPAddress.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.lblIPAddress.Location = new System.Drawing.Point(9, 203);
            this.lblIPAddress.Name = "lblIPAddress";
            this.lblIPAddress.Size = new System.Drawing.Size(224, 15);
            this.lblIPAddress.Text = "当前设备IP：";
            this.lblIPAddress.Visible = false;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(102, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(135, 41);
            this.label6.Text = "服務器設置 - \r\n    請填寫與SAP WEB SERVICE相關的訊息。";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.SteelBlue;
            this.label7.Location = new System.Drawing.Point(0, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(239, 4);
            // 
            // btnSQLSave
            // 
            this.btnSQLSave.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.btnSQLSave.Location = new System.Drawing.Point(132, 257);
            this.btnSQLSave.Name = "btnSQLSave";
            this.btnSQLSave.Size = new System.Drawing.Size(101, 25);
            this.btnSQLSave.TabIndex = 40;
            this.btnSQLSave.Text = "保存設定";
            this.btnSQLSave.Click += new System.EventHandler(this.btnSQLSave_Click);
            // 
            // tbxPassword
            // 
            this.tbxPassword.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.tbxPassword.Location = new System.Drawing.Point(95, 139);
            this.tbxPassword.Name = "tbxPassword";
            this.tbxPassword.PasswordChar = '*';
            this.tbxPassword.Size = new System.Drawing.Size(138, 22);
            this.tbxPassword.TabIndex = 14;
            this.tbxPassword.Text = "porite001";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(9, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 15);
            this.label4.Text = "密碼：";
            // 
            // tbxUser
            // 
            this.tbxUser.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.tbxUser.Location = new System.Drawing.Point(95, 112);
            this.tbxUser.Name = "tbxUser";
            this.tbxUser.Size = new System.Drawing.Size(138, 22);
            this.tbxUser.TabIndex = 11;
            this.tbxUser.Text = "CNRFC";
            this.tbxUser.GotFocus += new System.EventHandler(this.tbxUser_GotFocus);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(9, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 15);
            this.label3.Text = "用戶名：";
            // 
            // tbxPort
            // 
            this.tbxPort.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.tbxPort.Location = new System.Drawing.Point(95, 86);
            this.tbxPort.Name = "tbxPort";
            this.tbxPort.Size = new System.Drawing.Size(138, 22);
            this.tbxPort.TabIndex = 7;
            this.tbxPort.Text = "8000";
            this.tbxPort.GotFocus += new System.EventHandler(this.tbxDatabase_GotFocus);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(9, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 15);
            this.label2.Text = "服務器端口：";
            // 
            // tbxServer
            // 
            this.tbxServer.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.tbxServer.Location = new System.Drawing.Point(95, 60);
            this.tbxServer.Name = "tbxServer";
            this.tbxServer.Size = new System.Drawing.Size(138, 22);
            this.tbxServer.TabIndex = 6;
            this.tbxServer.Text = "10.5.20.14";
            this.tbxServer.GotFocus += new System.EventHandler(this.tbxServer_GotFocus);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(9, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 15);
            this.label1.Text = "服務器地址：";
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "名稱";
            this.columnHeader1.Width = 100;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(4, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(94, 43);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // lblHint
            // 
            this.lblHint.BackColor = System.Drawing.Color.Red;
            this.lblHint.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.lblHint.ForeColor = System.Drawing.Color.White;
            this.lblHint.Location = new System.Drawing.Point(3, 230);
            this.lblHint.Name = "lblHint";
            this.lblHint.Size = new System.Drawing.Size(233, 23);
            this.lblHint.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // listView1
            // 
            this.listView1.Columns.Add(this.columnHeader1);
            this.listView1.Columns.Add(this.columnHeader2);
            this.listView1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular);
            this.listView1.FullRowSelect = true;
            this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView1.Location = new System.Drawing.Point(9, 71);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(224, 112);
            this.listView1.TabIndex = 45;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "編碼";
            this.columnHeader2.Width = 120;
            // 
            // btnDeleteArea
            // 
            this.btnDeleteArea.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.btnDeleteArea.Location = new System.Drawing.Point(9, 240);
            this.btnDeleteArea.Name = "btnDeleteArea";
            this.btnDeleteArea.Size = new System.Drawing.Size(101, 25);
            this.btnDeleteArea.TabIndex = 46;
            this.btnDeleteArea.Text = "刪除區域";
            this.btnDeleteArea.Click += new System.EventHandler(this.btnDeleteArea_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnInitPD);
            this.tabPage3.Controls.Add(this.btnDeleteArea);
            this.tabPage3.Controls.Add(this.listView1);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.btnSaveArea);
            this.tabPage3.Controls.Add(this.tbxPDCode);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.tbxPDArea);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.pictureBox3);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Location = new System.Drawing.Point(0, 0);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(246, 296);
            this.tabPage3.Text = "盤點區域";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label15.Location = new System.Drawing.Point(9, 55);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(120, 15);
            this.label15.Text = "現有盤點區域：";
            // 
            // btnSaveArea
            // 
            this.btnSaveArea.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.btnSaveArea.Location = new System.Drawing.Point(132, 240);
            this.btnSaveArea.Name = "btnSaveArea";
            this.btnSaveArea.Size = new System.Drawing.Size(101, 25);
            this.btnSaveArea.TabIndex = 42;
            this.btnSaveArea.Text = "保存區域";
            this.btnSaveArea.Click += new System.EventHandler(this.btnSaveArea_Click);
            // 
            // tbxPDCode
            // 
            this.tbxPDCode.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.tbxPDCode.Location = new System.Drawing.Point(102, 211);
            this.tbxPDCode.Name = "tbxPDCode";
            this.tbxPDCode.Size = new System.Drawing.Size(131, 22);
            this.tbxPDCode.TabIndex = 11;
            this.tbxPDCode.GotFocus += new System.EventHandler(this.tbxPDCode_GotFocus);
            this.tbxPDCode.LostFocus += new System.EventHandler(this.tbxPDCode_LostFocus);
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label14.Location = new System.Drawing.Point(9, 214);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(120, 15);
            this.label14.Text = "盤點區域編碼：";
            // 
            // tbxPDArea
            // 
            this.tbxPDArea.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.tbxPDArea.Location = new System.Drawing.Point(102, 186);
            this.tbxPDArea.Name = "tbxPDArea";
            this.tbxPDArea.Size = new System.Drawing.Size(131, 22);
            this.tbxPDArea.TabIndex = 8;
            this.tbxPDArea.GotFocus += new System.EventHandler(this.tbxPDArea_GotFocus);
            this.tbxPDArea.LostFocus += new System.EventHandler(this.tbxPDArea_LostFocus);
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label13.Location = new System.Drawing.Point(9, 189);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(120, 15);
            this.label13.Text = "盤點區域名稱：";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(4, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(94, 43);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(102, 4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(135, 41);
            this.label11.Text = "盤點區域設定 - \r\n    可刪除、新增盤點區域";
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.SteelBlue;
            this.label12.Location = new System.Drawing.Point(0, 46);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(239, 4);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.button1.Location = new System.Drawing.Point(9, 257);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 25);
            this.button1.TabIndex = 43;
            this.button1.Text = "列印測試";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnSaveCoord);
            this.tabPage2.Controls.Add(this.tbxYCoord);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.tbxXCoord);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.listView2);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.pictureBox2);
            this.tabPage2.Controls.Add(this.lblHint);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.btnBTSave);
            this.tabPage2.Controls.Add(this.tbxBTAddress);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Location = new System.Drawing.Point(0, 0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(246, 296);
            this.tabPage2.Text = "列印機設定";
            // 
            // btnSaveCoord
            // 
            this.btnSaveCoord.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.btnSaveCoord.Location = new System.Drawing.Point(135, 257);
            this.btnSaveCoord.Name = "btnSaveCoord";
            this.btnSaveCoord.Size = new System.Drawing.Size(101, 25);
            this.btnSaveCoord.TabIndex = 68;
            this.btnSaveCoord.Text = "保存座標";
            this.btnSaveCoord.Click += new System.EventHandler(this.btnSaveCoord_Click);
            // 
            // tbxYCoord
            // 
            this.tbxYCoord.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.tbxYCoord.Location = new System.Drawing.Point(175, 205);
            this.tbxYCoord.Name = "tbxYCoord";
            this.tbxYCoord.Size = new System.Drawing.Size(58, 22);
            this.tbxYCoord.TabIndex = 59;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label17.Location = new System.Drawing.Point(123, 209);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 15);
            this.label17.Text = "Y座標：";
            // 
            // tbxXCoord
            // 
            this.tbxXCoord.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.tbxXCoord.Location = new System.Drawing.Point(61, 205);
            this.tbxXCoord.Name = "tbxXCoord";
            this.tbxXCoord.Size = new System.Drawing.Size(58, 22);
            this.tbxXCoord.TabIndex = 56;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label10.Location = new System.Drawing.Point(9, 209);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 15);
            this.label10.Text = "X座標：";
            // 
            // listView2
            // 
            this.listView2.Columns.Add(this.columnHeader3);
            this.listView2.Columns.Add(this.columnHeader4);
            this.listView2.Columns.Add(this.columnHeader5);
            this.listView2.FullRowSelect = true;
            listViewItem17.Text = "编码";
            listViewItem17.SubItems.Add("1");
            listViewItem17.SubItems.Add("2");
            listViewItem18.Text = "名稱";
            listViewItem19.Text = "供應商批次";
            listViewItem20.Text = "入庫月份";
            listViewItem21.Text = "數量";
            listViewItem22.Text = "Label ID";
            listViewItem23.Text = "序列號";
            listViewItem24.Text = "QRCODE";
            listViewItem25.Text = "999";
            listViewItem26.Text = "編碼列印值";
            listViewItem27.Text = "名稱列印值";
            listViewItem28.Text = "批次列印值";
            listViewItem29.Text = "月分列印值";
            listViewItem30.Text = "數量列印值";
            listViewItem31.Text = "LABELID列印值";
            listViewItem32.Text = "QRCODE列印值";
            this.listView2.Items.Add(listViewItem17);
            this.listView2.Items.Add(listViewItem18);
            this.listView2.Items.Add(listViewItem19);
            this.listView2.Items.Add(listViewItem20);
            this.listView2.Items.Add(listViewItem21);
            this.listView2.Items.Add(listViewItem22);
            this.listView2.Items.Add(listViewItem23);
            this.listView2.Items.Add(listViewItem24);
            this.listView2.Items.Add(listViewItem25);
            this.listView2.Items.Add(listViewItem26);
            this.listView2.Items.Add(listViewItem27);
            this.listView2.Items.Add(listViewItem28);
            this.listView2.Items.Add(listViewItem29);
            this.listView2.Items.Add(listViewItem30);
            this.listView2.Items.Add(listViewItem31);
            this.listView2.Items.Add(listViewItem32);
            this.listView2.Location = new System.Drawing.Point(3, 89);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(230, 110);
            this.listView2.TabIndex = 54;
            this.listView2.View = System.Windows.Forms.View.Details;
            this.listView2.SelectedIndexChanged += new System.EventHandler(this.listView2_SelectedIndexChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "列印內容";
            this.columnHeader3.Width = 100;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "X座標";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 60;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Y座標";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 60;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.SteelBlue;
            this.label18.Location = new System.Drawing.Point(0, 82);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(239, 4);
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(102, 4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(135, 41);
            this.label8.Text = "列印機設置 - \r\n    請填寫正確的標籤列印機的藍牙地址";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.SteelBlue;
            this.label9.Location = new System.Drawing.Point(0, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(239, 4);
            // 
            // btnBTSave
            // 
            this.btnBTSave.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.btnBTSave.Location = new System.Drawing.Point(190, 55);
            this.btnBTSave.Name = "btnBTSave";
            this.btnBTSave.Size = new System.Drawing.Size(46, 25);
            this.btnBTSave.TabIndex = 41;
            this.btnBTSave.Text = "保存";
            this.btnBTSave.Click += new System.EventHandler(this.btnBTSave_Click);
            // 
            // tbxBTAddress
            // 
            this.tbxBTAddress.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.tbxBTAddress.Location = new System.Drawing.Point(95, 57);
            this.tbxBTAddress.Name = "tbxBTAddress";
            this.tbxBTAddress.Size = new System.Drawing.Size(93, 22);
            this.tbxBTAddress.TabIndex = 8;
            this.tbxBTAddress.GotFocus += new System.EventHandler(this.tbxBTAddress_GotFocus);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label5.Location = new System.Drawing.Point(9, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 15);
            this.label5.Text = "列印機地址：";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(246, 322);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(0, 0);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(238, 295);
            this.tabPage4.Text = "退出設定";
            // 
            // btnInitPD
            // 
            this.btnInitPD.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.btnInitPD.Location = new System.Drawing.Point(9, 268);
            this.btnInitPD.Name = "btnInitPD";
            this.btnInitPD.Size = new System.Drawing.Size(101, 25);
            this.btnInitPD.TabIndex = 53;
            this.btnInitPD.Text = "盤點初始化";
            this.btnInitPD.Click += new System.EventHandler(this.btnInitPD_Click);
            // 
            // ParameterFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(246, 322);
            this.Controls.Add(this.tabControl1);
            this.MinimizeBox = false;
            this.Name = "ParameterFrm";
            this.Text = "參數設定";
            this.Load += new System.EventHandler(this.ParameterFrm_Load);
            this.tabPage1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.WindowsCE.Forms.InputPanel inputPanel1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblIPAddress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSQLSave;
        private System.Windows.Forms.TextBox tbxPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbxUser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbxPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxServer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblHint;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button btnDeleteArea;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnSaveArea;
        private System.Windows.Forms.TextBox tbxPDCode;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbxPDArea;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnBTSave;
        private System.Windows.Forms.TextBox tbxBTAddress;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button btnWebTest;
        private System.Windows.Forms.TextBox tbxClient;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.TextBox tbxYCoord;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbxXCoord;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnSaveCoord;
        private System.Windows.Forms.Button btnInitPD;
    }
}