﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Runtime.InteropServices;
using Microsoft.WindowsCE.Forms;
using System.Windows;
using ZSDK_API.Printer;
using ZSDK_API.Comm;
using ZSDK_API.ApiException;
using System.Threading;
using System.Net;
using Intermec.DataCollection;

namespace PoriteTW
{
    //public delegate void PrinterConnectionHandler();

    public partial class PrintFrm : Form
    {
        private Intermec.DataCollection.BarcodeReader bcr;

        public event PrinterConnectionHandler connectionEstablished;
        public event PrinterConnectionHandler connectionClosed;
        private delegate void StatusEventHandler(StatusArguments e);

        // 標籤中固定文本內容座標
        private int Material_X = 0;
        private int Material_Y = 0;
        private int Maktx_X = 0;
        private int Maktx_Y = 0;
        private int Lota_X = 0;
        private int Lota_Y = 0;
        private int RDate_X = 0;
        private int RDate_Y = 0;
        private int QTY_X = 0;
        private int QTY_Y = 0;
        private int GAL_X = 0;
        private int GAL_Y = 0;
        private int LabelID_X = 0;
        private int LabelID_Y = 0;
        private int Serail_X = 0;
        private int Serial_Y = 0;
        private int QRCODE_X = 0;
        private int QRCODE_Y = 0;
        private int Mark_X = 0;
        private int Mark_Y = 0;
        // 標籤中變量文本內容座標
        private int MaterialValue_X = 0;
        private int MaterialValue_Y = 0;
        private int MaktxValue_X = 0;
        private int MaktxValue_Y = 0;
        private int LotaValue_X = 0;
        private int LotaValue_Y = 0;
        private int RDateValue_X = 0;
        private int RDateValue_Y = 0;
        private int QTYValue_X = 0;
        private int QTYValue_Y = 0;
        private int LabelIDValue_X = 0;
        private int LabelIDValue_Y = 0;
        private int SerailValue_X = 0;
        private int SerialValue_Y = 0;

        private ZebraPrinter printer;
        private ZebraPrinterConnection connection;
        private string qrcode = "";
        private string ZPL = "";
        private bool BTPrint = false;     //当前打印机连接状态
        private int LabelCount = 0;

        int FLAG = 0;//判断标签是旧标签，还是新标签。

        private string ssname = "";
        private string ssuser = "";
        private string sspasswd = "";
        private string ssdatabase = "";

        private string btaddress = "";     // 蓝牙地址
        private string terminal = "";

        private string error = "";          // 错误信息

        private string SMaterial = "";      // 物料编号
        private string SMaktx = "";         // 物料描述
        private string SGlota = "";         // 供应商批次
        private string SQty = "";           // 数量
        private string SCunit="";           // 单位
        private string SLota = "";          // 厂内批次
        private string SRdate = "";         // 入库日期
        private string SSRdate = "";        // 带符号的入库日期
        private string SPDate ="";          // 打印日期
        private string SLbsn = "";          // 标签ID
        private string SLabelsn = "";       // 标签序列号
        private string LABELSN = "";        // 标签序列号
        private int LBSN = 0;               // 标签后两位数值

        private enum Flags
        {
            SND_SYNC = 0x0000,  /* play synchronously (default) */
            SND_ASYNC = 0x0001,  /* play asynchronously */
            SND_NODEFAULT = 0x0002,  /* silence (!default) if sound not found */
            SND_MEMORY = 0x0004,  /* pszSound points to a memory file */
            SND_LOOP = 0x0008,  /* loop the sound until next sndPlaySound */
            SND_NOSTOP = 0x0010,  /* don't stop any currently playing sound */
            SND_NOWAIT = 0x00002000, /* don't wait if the driver is busy */
            SND_ALIAS = 0x00010000, /* name is a registry alias */
            SND_ALIAS_ID = 0x00110000, /* alias is a predefined ID */
            SND_FILENAME = 0x00020000, /* name is file name */
            SND_RESOURCE = 0x00040004  /* name is resource name or atom */
        }

        [DllImport("coredll.dll", EntryPoint = "PlaySound", SetLastError = true)]
        private static extern int WCE_PlaySoundBytes(byte[] szSound, IntPtr hMod, int flags);

        public delegate void textbox_delegate(string msg);

        public PrintFrm()
        {
            InitializeComponent();
            connectionEstablished += new PrinterConnectionHandler(sendLabel);
            bcr = new BarcodeReader();
            bcr.BarcodeRead += new BarcodeReadEventHandler(bcr_BarcodeRead);
            bcr.ThreadedRead(true);
            bcr.ScannerEnable = true;
        }

        // 播放声音提示
        public static void PlayAudio(byte[] waveFile)
        {
            WCE_PlaySoundBytes(waveFile, IntPtr.Zero, (int)(Flags.SND_SYNC | Flags.SND_MEMORY));
        }

        // 打印数量初始为0
        public void PrnCount(string msg)
        {
            if (tbxPrntCount == null) return;
            if (tbxPrntCount.InvokeRequired)
            {
                textbox_delegate dt = new textbox_delegate(PrnCount);
                tbxPrntCount.Invoke(dt, new object[] { msg });
            }
            else
            {
                tbxPrntCount.Text = msg;
            }
        }

        // textbox控件操作
        public void textbox(string msg)
        {
            if (tbxBarcode == null) return;
            if (tbxBarcode.InvokeRequired)
            {
                textbox_delegate dt = new textbox_delegate(textbox);
                tbxBarcode.Invoke(dt, new object[] { msg });
            }
            else
            {
                tbxBarcode.Text = msg;//更新textbox内容
                tbxBarcode.Focus();
            }

        }

        public String BluetoothMacAddress
        {
            get
            {
                return textBox1.Text;
            }
            set
            {
                textBox1.Text = value;
            }
        }

        public void connect()
        {
            connectBluetooth(BluetoothMacAddress);
        }

        private String macAddress;

        private void connectBluetooth(String macAddress)
        {
            this.macAddress = macAddress.Trim();
            Thread t = new Thread(doConnectBT);
            t.Start();
        }

        private void doConnectBT()
        {
            if (this.macAddress.Length != 12)
            {
                BTPrint = false;
                updateGuiFromWorkerThread("無效的列印機 MAC 地址", Color.Red);
                disconnect();
            }
            else
            {
                updateGuiFromWorkerThread("列印機連接中...請稍候...", Color.Goldenrod);
                try
                {
                    connection = new BluetoothPrinterConnection(this.macAddress);
                    threadedConnect(this.macAddress);
                }
                catch (ZebraException)
                {
                    BTPrint = false;
                    updateGuiFromWorkerThread("列印機通訊失敗，請檢察PDA藍牙是否打開！", Color.Red);
                    connectionClosed();
                }
            }
        }

        public void disconnect()
        {
            Thread t = new Thread(doDisconnect);
            t.Start();
        }

        private void doDisconnect()
        {
            try
            {
                if (connection != null && connection.IsConnected())
                {
                    //updateGuiFromWorkerThread("断开连接！", Color.SkyBlue);
                    connection.Close();
                    Thread.Sleep(500);
                    updateGuiFromWorkerThread("列印完成，斷開連接！", Color.SkyBlue);
                    connection = null;
                    PrnCount("0");
                    textbox("");
                }
            }
            catch (ZebraException)
            {
                BTPrint = false;
                updateGuiFromWorkerThread("通訊錯誤，藍牙連接失敗！", Color.Red);
            }
        }
 
        //----------------------------------------------------------------------------
        public void SetStatus(String message, Color color)
        {
            label2.Text = message;
            label2.BackColor = color;
        }

        public void updateGuiFromWorkerThread(String message, Color color)
        {
            Invoke(new StatusEventHandler(UpdateUI), new StatusArguments(message, color));
        }

        private void UpdateUI(StatusArguments e)
        {
            SetStatus("狀態: " + e.message, e.color);
        }
        //------------------------------------------------------------------------------

        private class StatusArguments : System.EventArgs
        {
            public String message;
            public Color color;

            public StatusArguments(String message, Color color)
            {
                this.message = message;
                this.color = color;
            }
        }

        // 连接打印机线程
        private void threadedConnect(String addressName)
        {
            try
            {
                connection.Open();
                Thread.Sleep(1000);
            }
            catch (ZebraPrinterConnectionException)
            {
                BTPrint = false;
                updateGuiFromWorkerThread("無法連接到列印機，請檢測列印機！", Color.Red);
                disconnect();
            }
            catch (ZebraGeneralException e)
            {
                BTPrint = false;
                updateGuiFromWorkerThread(e.Message, Color.Red);
                disconnect();
            }
            catch (Exception)
            {
                BTPrint = false;
                updateGuiFromWorkerThread("列印機通訊錯誤！", Color.Red);
                disconnect();
            }

            printer = null;
            if (connection != null && connection.IsConnected())
            {
                try
                {
                    printer = ZebraPrinterFactory.GetInstance(connection);
                    PrinterLanguage pl = printer.GetPrinterControlLanguage();
                    updateGuiFromWorkerThread("準備打印中......", Color.Goldenrod);
                    Thread.Sleep(1000);
                    //updateGuiFromWorkerThread("Connected to " + addressName, Color.Green);
                    //Thread.Sleep(1000);
                    this.connectionEstablished();
                }
                catch (ZebraPrinterConnectionException)
                {
                    BTPrint = false;
                    updateGuiFromWorkerThread("列印機連接失敗！", Color.Red);
                    printer = null;
                    Thread.Sleep(1000);
                    disconnect();
                }
                catch (ZebraPrinterLanguageUnknownException)
                {
                    BTPrint = false;
                    updateGuiFromWorkerThread("無效的列印指令！", Color.Red);
                    printer = null;
                    Thread.Sleep(1000);
                    disconnect();
                }
            }
        }

        // 连接打印机
        public ZebraPrinterConnection getConnection()
        {
            return connection;
        }

        // 定义打印
        public ZebraPrinter getPrinter()
        {
            return printer;
        }

        // 打印指令字符集转换
        private byte[] getConfigLabel(string zpl)
        {
            return Encoding.GetEncoding("utf-8").GetBytes(zpl);
        }

        // 发送CPCL指令到斑马移动打印机
        public void sendLabel()
        {
            ZebraPrinter printer = getPrinter();
            if (printer != null)
            {
                for (int i = 1; i <= LabelCount; i++)
                {
                    LABELSN = (i + LBSN).ToString("D2");
                    qrcode = SMaterial + "/" + SGlota + "/" + SQty + "/" + SLabelsn + "/" + SRdate.Replace("/","") + "/" + LABELSN+"/"+SCunit;
                    ZPL = "";
                    ZPL = "";
                    ZPL += "^XA";
                    ZPL += "^LT0^LH0,0^MNY";
                    ZPL += "^CW1,E:ANMDS.TTF";
                    ZPL += "^CI28";
                    ZPL += "^FO" + QRCODE_X.ToString() + "," + QRCODE_Y.ToString();
                    ZPL += "^BQN,2,9^FDMA," + qrcode + "^FS";
                    ZPL += "^FT" + Material_X.ToString() + "," + Material_Y.ToString() + "^A1N,40,40^FD編碼:^FS";
                    ZPL += "^FT" + MaterialValue_X + "," + MaterialValue_Y + "^A1N,40,40^FD" + SMaterial + "^FS";

                    ZPL += "^FT" + Maktx_X.ToString() + "," + Maktx_Y.ToString() + "^A1N,40,40^FD名稱: " + "" + "^FS";
                    ZPL += "^FT" + MaktxValue_X.ToString() + "," + MaktxValue_Y.ToString() + "^A1N,24,24^FD" + SMaktx + "^FS";

                    ZPL += "^FT" + Lota_X.ToString() + "," + Lota_Y.ToString() + "^A1N,40,40^FD供應商批次:^FS";
                    ZPL += "^FT" + LotaValue_X.ToString() + "," + LotaValue_Y.ToString() + "^A1N,40,40^FD" + SGlota + "^FS";

                    ZPL += "^FT" + RDate_X.ToString() + "," + RDate_Y.ToString() + "^A1N,40,40^FD入庫月份:^FS";
                    ZPL += "^FT" + RDateValue_X.ToString() + "," + RDateValue_Y.ToString() + "^A1N,40,40^FD" + SRdate + "^FS";

                    ZPL += "^FT" + QTY_X.ToString() + "," + QTY_Y.ToString() + "^A1N,40,40^FD數量:^FS";
                    ZPL += "^FT" + QTYValue_X.ToString() + "," + QTYValue_Y.ToString() + "^A1N,40,40^FD" + SQty + "^FS";

                    ZPL += "^FT" + GAL_X.ToString() + "," + GAL_Y.ToString() + "^A1N,40,40^FD" + SCunit + "^FS";

                    ZPL += "^FT" + LabelID_X.ToString() + "," + LabelID_Y.ToString() + "^A1N,40,40^FDLABEL ID:^FS";
                    ZPL += "^FT" + LabelIDValue_X.ToString() + "," + LabelIDValue_Y.ToString() + "^A1N,40,40^FD" + SLabelsn + "^FS";

                    ZPL += "^FT" + Serail_X.ToString() + "," + Serial_Y.ToString() + "^A1N,40,40^FD序列號: ^FS";
                    ZPL += "^FT" + SerailValue_X.ToString() + "," + SerialValue_Y.ToString() + "^A1N,40,40^FD" + LABELSN + "^FS";

                    ZPL += "^FT" + Mark_X.ToString() + "," + Mark_Y.ToString() + "^A1N,40,40^FD999^FS";

                    ZPL += "^XZ";
                    byte[] configLabel = getConfigLabel(ZPL);
                    getConnection().Write(configLabel);
                    updateGuiFromWorkerThread("列印中...", Color.SkyBlue);
                    Thread.Sleep(1000);

                    //SPDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    //string SQL = "";
                    //SQL = "INSERT INTO REPLABEL VALUES('" + SMaterial + "','" + SMaktx + "','" + SGlota + "','" +
                    //                                  SQty + "','" + SCunit + "','" + SLota + "','" +
                    //                                  SRdate + "','" + SPDate + "','" + SLbsn + "','" + LABELSN + "',1)";
                    //if (FLAG == 0)
                    //{
                    //     SQL = "INSERT INTO REPLABEL VALUES('" + SMaterial + "','" + SMaktx + "','" + SLota + "','" +
                    //                                              SQty + "','" + SCunit + "','" + SGlota + "','" +
                    //                                              SRdate + "','" + SPDate + "','" + SLbsn + "','" + LABELSN + "',1)";
                    //}
                    //else
                    //{
                    //    SQL = "INSERT INTO REPLABEL VALUES('" + SMaterial + "','" + SMaktx + "','" + SGlota + "','" +
                    //                      SQty + "','" + SCunit + "','" + SLota + "','" +
                    //                      SRdate + "','" + SPDate + "','" + SLbsn + "','" + LABELSN + "',1)";
                    //}
                    
                }
            }
            disconnect();
        }

        // 根据月份显示对应符号
        private string GetDateSymbol(int d)
        {
            string Result = "";
            switch (d)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    Result = "△";
                    break;
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                    Result = "○";
                    break;

            }
            return Result;
        }

        // 显示二维码内容
        private void Show2D(string[] split)
        {
            // 物料号/批次号/数量/条码ID/日期/序列号/单位
            // 物料编码
            tbxMaterial.Text = split[0].ToString();
            SMaterial = split[0].ToString();
            // 供应商批次
            tbxGLota.Text = split[1].ToString();
            SGlota = split[1].ToString();
            // 数量
            tbxQty.Text = split[2].ToString();
            SQty = split[2].ToString();
            // 條碼ID
            tbxLota.Text = split[3].ToString();
            SLabelsn = split[3].ToString();
            //日期
            string temp = split[4].ToString();
            tbxRDate.Text = temp.Substring(0, 4) + "/" + temp.Substring(4, 2) + "/" + temp.Substring(6, 2);
            SRdate = tbxRDate.Text;
            // 單位
            tbxUnit.Text = split[6].ToString();
            SCunit = split[6].ToString();
        }

        // 处理扫描内容
        private void bcr_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            if (tbxBarcode.Focused)
            {
                tbxBarcode.Text = bre.strDataBuffer.ToString().Trim();
                if (bre.Symbology != 24)
                {
                    MessageBox.Show("當前掃描的條形碼不是有效的QRCODE，請重新掃描正確的QRCODE！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    return;
                }
                KeyPressEventArgs ek = new KeyPressEventArgs((char)Keys.Enter);
                tbxBarcode_KeyPress(tbxBarcode, ek);
                return;
            }
        }

        // 扫描二维码信息
        private void tbxBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                qrcode = tbxBarcode.Text;
                if (qrcode.Equals(""))
                {
                    MessageBox.Show("請掃描二維碼標籤！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                    PlayAudio(Properties.Resources.UNFUN);
                    return;
                }

                // 判断二维码是否为有效数据,如果是,则获取标签ID
                string[] split = qrcode.Split('/');
                if (split.Length != 7)
                {
                    MessageBox.Show("當前掃描的二維碼數據是無效數據，請重新掃描！", "訊息");
                    PlayAudio(Properties.Resources.UNFUN);
                    tbxBarcode.Text = "";
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;
                Show2D(split);
                try
                {
                    // 从重打表中获取当前标签ID最大序列号
                    System.Net.NetworkCredential c = new System.Net.NetworkCredential(UDF.SAP_USER, UDF.SAP_PASS);
                    SAPWR.Z_RAW_MATERIAL_LABEL_COPY func = new PoriteTW.SAPWR.Z_RAW_MATERIAL_LABEL_COPY();
                    func.Credentials = c;
                    func.SoapVersion = System.Web.Services.Protocols.SoapProtocolVersion.Soap12;
                    func.Url = "http://" + UDF.SAP_SERVER + ":" + UDF.SAP_PORT + "/sap/bc/srt/rfc/sap/z_raw_material_label_copy/" + UDF.SAP_CLIENT + "/z_raw_material_label_copy/z_raw_material_label_copy";
                    //定义接口所需要参数
                    SAPWR.ZRawMaterialLabelCopy copy = new PoriteTW.SAPWR.ZRawMaterialLabelCopy();
                    copy.Material = SMaterial;
                    copy.Plant = "PE01";
                    copy.ZbatchNo = SGlota;
                    copy.Zlabelid = SLabelsn;
                    SAPWR.ZRawMaterialLabelCopyResponse resp = func.ZRawMaterialLabelCopy(copy);
                    // 物料描述
                    SMaktx = resp.MaterialTx;
                    // 标签序列号
                    LABELSN = resp.Versio;
                    tbxMaktx.Text = SMaktx;
                }
                catch (WebException wex)
                {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("異常訊息:\n\r" + wex.Message + "\n\r請檢查網路是否正常！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    return;
                }
                Cursor.Current = Cursors.Default;
                // 显示出剩余可打印数量
                int sn = 0;
                sn = Convert.ToInt32(LABELSN);
                tbxRemainder.Text = (99 - sn).ToString();
                tbxPrntCount.Focus();
                tbxPrntCount.SelectAll();
                PlayAudio(Properties.Resources.OperateSuccess);
            }
        }

        // 窗体加载事件
        private void PrintFrm_Load(object sender, EventArgs e)
        {
            tbxBarcode.Focus();
            //UDF.GetSERVER(ref ssname, ref ssuser, ref sspasswd, ref ssdatabase);
            UDF.GetSAPWS(ref UDF.SAP_SERVER, ref UDF.SAP_PORT, ref UDF.SAP_USER, ref UDF.SAP_PASS, ref UDF.SAP_CLIENT);
            UDF.GetBluetooth(ref btaddress,ref terminal);
            textBox1.Text = btaddress;
            if (textBox1.Text.Equals(""))
            {
                MessageBox.Show("當前列印機藍牙地址沒有設定，請先設定列印機藍牙地址后再使用！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                this.Close();
                return;
            }
            this.WindowState = FormWindowState.Maximized;
        }

        // 不用：打印二维码
        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (tbxBarcode.Text.Equals(""))
            {
                MessageBox.Show("请扫描二维码后再打印！", "提示");
                
                return;
            }
            if (tbxBarcode.Text.Split('/').Length != 5)
            {
                MessageBox.Show("当前二维码无效，请扫描有效的二维码再打印！", "提示");
                return;
            }
            if (LabelCount == 0)
            {
                MessageBox.Show("当前打印数量不能为0！", "提示");
                return;
            }
            qrcode = tbxBarcode.Text.Trim();
            string s1 = "";
            string s2 = "";
            string s3 = "";
            string s4 = "";
            string s5 = "";

            string[] temp = qrcode.Split('/');
            s1 = temp[0];
            s2 = temp[1];
            s3 = temp[2];
            s4 = temp[3];
            s5 = temp[4];
            string s = "";
            //s += "^XA^JST^XZ";
            //s += "^XA";
            //s += "^CW1,E:MSSUNG24.FNT";
            //s += "^CI28";
            //s += "^FO200,0";
            //s += "^BQN,2,9^FDLA," + qrcode + "^FS";
            //s += "^FO178,372^A1N,34,34^FD物料编码: " + s1 + "^FS";
            //s += "^FO178,431^A1N,34,34^FD物料名称: " + s1 + "^FS";
            //s += "^FO178,490^A1N,34,34^FD生产批号: " + s2 + "^FS";
            //s += "^FO178,549^A1N,34,34^FD入库月份: " + s3 + "^FS";
            //s += "^FO178,608^A1N,34,34^FD生产数量: " + s4 + "^FS";
            //s += "^FO178,667^A1N,34,34^FD条码  ID: " + s5 + "^FS";
            //s += "^XZ";
            s += "! 0 200 200 900 " + LabelCount + "\r\n";
            s += "LABEL\r\n";
            s += "PAGE-WIDTH 600\r\n";
            s += "B QR 150 10 M 2 U 10\r\n";
            s += "MA, " + qrcode + "\r\n";
            s += "ENDQR\r\n";
            s += "SETMAG 1 1\r\n";
            s += "COUNTRY CHINA\r\n";
            s += "T 55 0 30 346 物料编码:\r\n";
            s += "T 4 0 160 330 " + s1 + "\r\n";
            s += "T 55 0 30 405 物料名称:\r\n";
            s += "T 4 0 160 389 " + s1 + "\r\n";
            s += "T 55 0 30 464 生产批号:\r\n";
            s += "T 4 0 160 448 " + s2 + "\r\n";
            s += "T 55 0 30 523 入库月份:\r\n";
            s += "T 4 0 160 507 " + s5 + "\r\n";
            s += "T 55 0 30 582 生产数量:\r\n";
            s += "T 4 0 160 566 " + s3 + "\r\n";
            s += "T 55 0 30 641 条码 ID:\r\n";
            s += "T 4 0 160 625 " + s4 + "\r\n";
            s += "FORM\r\n";
            s += "PRINT\r\n";
            //s += "! 0 200 200 900 1\r\n" +
            //     "LABEL\r\n" +
            //     "CONTRAST 0\r\n" +
            //     "SPEED 4\r\n" +
            //     "PAGE-WIDTH 640\r\n" +
            //        "B QR 100 10 M 2 U 6\r\n" +
            //        "M0A,QR code ABC123\r\n" +
            //        "ENDQR\r\n" +
            //        "T 4 0 10 150 QR code ABC123\r\n" +
            //        "SETMAG 2 2\r\n" +
            //        "COUNTRY CHINA\r\n" +
            //        "T 55 0 30 10 斑马技术公司 上海\r\n" +
            //        "T 55 0 30 100 斑马热敏式式移动打印机\r\n" +
            //        "FORM\r\n" +
            //        "PRINT\r\n";

            ZPL = s;
            connect();
            tbxBarcode.Text = "";
            tbxBarcode.Focus();
        }

        // 退出操作
        private void btnExit_Click(object sender, EventArgs e)
        {
            bcr.ThreadedRead(false);
            bcr.ScannerEnable = false;
            bcr.Dispose();
            this.Close();
        }

        // 打印数量输入
        private void tbxPrntCount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(e.KeyChar == (char)8) && !(Char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)13))
            {
                e.Handled = true;
            }
            else
            {
                LabelCount = Convert.ToInt32(tbxPrntCount.Text.Trim());
                if (LabelCount > 1)
                {
                    MessageBox.Show("當前輸入的列印數量不能大於1，請重新輸入！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                    return;
                }
                // 获取序列号后两位
                //int sn = 0;
                LBSN = Convert.ToInt32(LABELSN);
                if ((LabelCount + LBSN) > 99)
                {
                    MessageBox.Show("當前輸入列印數量已經超出可列印最大數量，請重新輸入！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                    tbxPrntCount.Focus();
                    tbxPrntCount.SelectAll();
                    return;
                }
                // 显示出剩余可打印数量
                tbxRemainder.Text = (99 - LabelCount - LBSN).ToString();
                // 讀取座標
                DataSet ds = UDF.GetLabelCoord();

                Material_X = Convert.ToInt32(ds.Tables[0].Rows[0][3].ToString());
                Material_Y = Convert.ToInt32(ds.Tables[0].Rows[0][4].ToString());
                Maktx_X = Convert.ToInt32(ds.Tables[0].Rows[1][3].ToString());
                Maktx_Y = Convert.ToInt32(ds.Tables[0].Rows[1][4].ToString());
                Lota_X = Convert.ToInt32(ds.Tables[0].Rows[2][3].ToString());
                Lota_Y = Convert.ToInt32(ds.Tables[0].Rows[2][4].ToString());
                RDate_X = Convert.ToInt32(ds.Tables[0].Rows[3][3].ToString());
                RDate_Y = Convert.ToInt32(ds.Tables[0].Rows[3][4].ToString());
                QTY_X = Convert.ToInt32(ds.Tables[0].Rows[4][3].ToString());
                QTY_Y = Convert.ToInt32(ds.Tables[0].Rows[4][4].ToString());
                GAL_X = Convert.ToInt32(ds.Tables[0].Rows[5][3].ToString());
                GAL_Y = Convert.ToInt32(ds.Tables[0].Rows[5][4].ToString());
                LabelID_X = Convert.ToInt32(ds.Tables[0].Rows[6][3].ToString());
                LabelID_Y = Convert.ToInt32(ds.Tables[0].Rows[6][4].ToString());
                Serail_X = Convert.ToInt32(ds.Tables[0].Rows[7][3].ToString());
                Serial_Y = Convert.ToInt32(ds.Tables[0].Rows[7][4].ToString());
                QRCODE_X = Convert.ToInt32(ds.Tables[0].Rows[8][3].ToString());
                QRCODE_Y = Convert.ToInt32(ds.Tables[0].Rows[8][4].ToString());
                Mark_X = Convert.ToInt32(ds.Tables[0].Rows[9][3].ToString());
                Mark_Y = Convert.ToInt32(ds.Tables[0].Rows[9][4].ToString());

                MaterialValue_X = Convert.ToInt32(ds.Tables[0].Rows[10][3].ToString());
                MaterialValue_Y = Convert.ToInt32(ds.Tables[0].Rows[10][4].ToString());
                MaktxValue_X = Convert.ToInt32(ds.Tables[0].Rows[11][3].ToString());
                MaktxValue_Y = Convert.ToInt32(ds.Tables[0].Rows[11][4].ToString());
                LotaValue_X = Convert.ToInt32(ds.Tables[0].Rows[12][3].ToString());
                LotaValue_Y = Convert.ToInt32(ds.Tables[0].Rows[12][4].ToString());
                RDateValue_X = Convert.ToInt32(ds.Tables[0].Rows[13][3].ToString());
                RDateValue_Y = Convert.ToInt32(ds.Tables[0].Rows[13][4].ToString());
                QTYValue_X = Convert.ToInt32(ds.Tables[0].Rows[14][3].ToString());
                QTYValue_Y = Convert.ToInt32(ds.Tables[0].Rows[14][4].ToString());
                LabelIDValue_X = Convert.ToInt32(ds.Tables[0].Rows[15][3].ToString());
                LabelIDValue_Y = Convert.ToInt32(ds.Tables[0].Rows[15][4].ToString());
                SerailValue_X = Convert.ToInt32(ds.Tables[0].Rows[16][3].ToString());
                SerialValue_Y = Convert.ToInt32(ds.Tables[0].Rows[16][4].ToString());
                // 创建可打印标签内容,并打印标签
                connect();
            }
        }


    }
}