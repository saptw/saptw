﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.WindowsCE.Forms;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Net;
using System.IO;
using Intermec.DataCollection;

namespace PoriteTW
{
    public delegate void PrinterConnectionHandler();

    public partial class MainFrm : Form
    {
        private string dir = @"\盤點";
        private bool IMChange = false;

        private Intermec.DataCollection.BarcodeReader bcr;

        private SYSTEM_POWER_STATUS_EX status = new SYSTEM_POWER_STATUS_EX();

        public const uint POWER_FORCE = 0x00001000u;
        public const uint POWER_STATE_RESET = 0x00800000u;        // reset state

        private class SYSTEM_POWER_STATUS_EX
        {
            public byte ACLineStatus = 0;
            public byte BatteryFlag = 0;
            public byte BatteryLifePercent = 0;
            public byte Reserved1 = 0;
            public uint BatteryLifeTime = 0;
            public uint BatteryFullLifeTime = 0;
            public byte Reserved2 = 0;
            public byte BackupBatteryFlag = 0;
            public byte BackupBatteryLifePercent = 0;
            public byte Reserved3 = 0;
            public uint BackupBatteryLifeTime = 0;
            public uint BackupBatteryFullLifeTime = 0;
        }

        public const int BacklightOff = 0x10010000;  // 关闭背光     
        public const int ColdReboot = 0x00800000;    // 重启         
        public const int On = 0x12010000;            // 打开电源     
        public const int Reboot = 0x00800000;        // 重启         
        public const int Resuming = 0x10000000;      // 系统正在恢复 
        public const int ScreenOff = 0x00100000;     // 关闭屏幕           
        public const int Suspend = 0x00200000;       // 系统休眠            
        public const int Unattended = 0x00400000;    //             
        public const int UserIdle = 0x01000000;      // 用户空闲 

        [DllImport("coredll")]
        private static extern int GetSystemPowerStatusEx(SYSTEM_POWER_STATUS_EX lpSystemPowerStatus, bool fUpdate);

        [DllImport("coredll.dll")]
        public static extern uint SetSystemPowerState([MarshalAs(UnmanagedType.LPWStr)]string psState, uint StateFlags, uint Options);

        [DllImport("user32.dll ", EntryPoint = "SendMessage ")]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, int wParam, string lParam);

        public MainFrm()
        {
            InitializeComponent();
            //InputMethod("e0010409");
        }

        private void MainFrm_Load(object sender, EventArgs e)
        {
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            //屏蔽系统任务栏
            Rectangle rectangle = Screen.PrimaryScreen.Bounds;
            UDF.SetFullScreen(true, ref rectangle);
            statusBar1.Text = "版本號：V" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(); ;
        }

        private void MainFrm_Closing(object sender, CancelEventArgs e)
        {
            //显示系统任务栏
            Rectangle rectangle = Screen.PrimaryScreen.Bounds;
            UDF.SetFullScreen(false, ref rectangle);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            bcr = new BarcodeReader();
            bcr.ThreadedRead(true);
            bcr.ScannerEnable = true;
            this.Close();
        }

        // 盤點操作
        private void btnPD_Click(object sender, EventArgs e)
        {
            PDFrm pdf = new PDFrm();
            pdf.ShowDialog();
        }

        // 标签复制
        private void btnPrint_Click(object sender, EventArgs e)
        {
            PrintFrm prf = new PrintFrm();
            prf.ShowDialog();
        }

        // 参数设置
        private void btnParameter_Click(object sender, EventArgs e)
        {
            ParameterFrm pfrm = new ParameterFrm();
            pfrm.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProductPDFrm ppfrm = new ProductPDFrm();
            ppfrm.ShowDialog();
        }
    }
}