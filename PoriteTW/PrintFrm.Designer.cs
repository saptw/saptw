﻿namespace PoriteTW
{
    partial class PrintFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.tbxBarcode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tbxPrntCount = new System.Windows.Forms.TextBox();
            this.tbxRemainder = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxMaterial = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbxMaktx = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbxGLota = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbxLota = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbxQty = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbxUnit = new System.Windows.Forms.TextBox();
            this.tbxRDate = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Red;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(246, 23);
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tbxBarcode
            // 
            this.tbxBarcode.Location = new System.Drawing.Point(75, 49);
            this.tbxBarcode.Name = "tbxBarcode";
            this.tbxBarcode.Size = new System.Drawing.Size(160, 21);
            this.tbxBarcode.TabIndex = 8;
            this.tbxBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxBarcode_KeyPress);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(1, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 20);
            this.label3.Text = "掃描條碼:";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(75, 25);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(160, 21);
            this.textBox1.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(1, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 15);
            this.label1.Text = "列印機：";
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(116, 268);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(97, 31);
            this.btnPrint.TabIndex = 13;
            this.btnPrint.Text = "打印标签";
            this.btnPrint.Visible = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(140, 268);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(97, 31);
            this.btnExit.TabIndex = 14;
            this.btnExit.Text = "退出";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(5, 218);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 20);
            this.label4.Text = "標籤列印數量:";
            // 
            // tbxPrntCount
            // 
            this.tbxPrntCount.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.tbxPrntCount.Location = new System.Drawing.Point(5, 236);
            this.tbxPrntCount.Name = "tbxPrntCount";
            this.tbxPrntCount.Size = new System.Drawing.Size(108, 22);
            this.tbxPrntCount.TabIndex = 20;
            this.tbxPrntCount.Text = "1";
            this.tbxPrntCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxPrntCount_KeyPress);
            // 
            // tbxRemainder
            // 
            this.tbxRemainder.BackColor = System.Drawing.Color.SkyBlue;
            this.tbxRemainder.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.tbxRemainder.ForeColor = System.Drawing.Color.Red;
            this.tbxRemainder.Location = new System.Drawing.Point(128, 236);
            this.tbxRemainder.Name = "tbxRemainder";
            this.tbxRemainder.ReadOnly = true;
            this.tbxRemainder.Size = new System.Drawing.Size(108, 22);
            this.tbxRemainder.TabIndex = 21;
            this.tbxRemainder.Text = "1";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label5.Location = new System.Drawing.Point(128, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 20);
            this.label5.Text = "可列印數量";
            // 
            // tbxMaterial
            // 
            this.tbxMaterial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbxMaterial.ForeColor = System.Drawing.Color.Red;
            this.tbxMaterial.Location = new System.Drawing.Point(75, 73);
            this.tbxMaterial.Name = "tbxMaterial";
            this.tbxMaterial.ReadOnly = true;
            this.tbxMaterial.Size = new System.Drawing.Size(160, 21);
            this.tbxMaterial.TabIndex = 64;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label6.Location = new System.Drawing.Point(1, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 15);
            this.label6.Text = "物料編碼：";
            // 
            // tbxMaktx
            // 
            this.tbxMaktx.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbxMaktx.ForeColor = System.Drawing.Color.Red;
            this.tbxMaktx.Location = new System.Drawing.Point(75, 97);
            this.tbxMaktx.Name = "tbxMaktx";
            this.tbxMaktx.ReadOnly = true;
            this.tbxMaktx.Size = new System.Drawing.Size(160, 21);
            this.tbxMaktx.TabIndex = 67;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label7.Location = new System.Drawing.Point(1, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 15);
            this.label7.Text = "物料名稱：";
            // 
            // tbxGLota
            // 
            this.tbxGLota.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbxGLota.ForeColor = System.Drawing.Color.Red;
            this.tbxGLota.Location = new System.Drawing.Point(75, 121);
            this.tbxGLota.Name = "tbxGLota";
            this.tbxGLota.ReadOnly = true;
            this.tbxGLota.Size = new System.Drawing.Size(160, 21);
            this.tbxGLota.TabIndex = 70;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label8.Location = new System.Drawing.Point(1, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 15);
            this.label8.Text = "批次：";
            // 
            // tbxLota
            // 
            this.tbxLota.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbxLota.ForeColor = System.Drawing.Color.Red;
            this.tbxLota.Location = new System.Drawing.Point(75, 145);
            this.tbxLota.Name = "tbxLota";
            this.tbxLota.ReadOnly = true;
            this.tbxLota.Size = new System.Drawing.Size(160, 21);
            this.tbxLota.TabIndex = 73;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label9.Location = new System.Drawing.Point(1, 148);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 15);
            this.label9.Text = "條碼ID：";
            // 
            // tbxQty
            // 
            this.tbxQty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbxQty.ForeColor = System.Drawing.Color.Red;
            this.tbxQty.Location = new System.Drawing.Point(75, 193);
            this.tbxQty.Name = "tbxQty";
            this.tbxQty.ReadOnly = true;
            this.tbxQty.Size = new System.Drawing.Size(84, 21);
            this.tbxQty.TabIndex = 76;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label10.Location = new System.Drawing.Point(1, 196);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 15);
            this.label10.Text = "數量：";
            // 
            // tbxUnit
            // 
            this.tbxUnit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbxUnit.ForeColor = System.Drawing.Color.Red;
            this.tbxUnit.Location = new System.Drawing.Point(162, 193);
            this.tbxUnit.Name = "tbxUnit";
            this.tbxUnit.ReadOnly = true;
            this.tbxUnit.Size = new System.Drawing.Size(73, 21);
            this.tbxUnit.TabIndex = 78;
            // 
            // tbxRDate
            // 
            this.tbxRDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tbxRDate.ForeColor = System.Drawing.Color.Red;
            this.tbxRDate.Location = new System.Drawing.Point(75, 169);
            this.tbxRDate.Name = "tbxRDate";
            this.tbxRDate.ReadOnly = true;
            this.tbxRDate.Size = new System.Drawing.Size(160, 21);
            this.tbxRDate.TabIndex = 90;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular);
            this.label11.Location = new System.Drawing.Point(1, 172);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 15);
            this.label11.Text = "日期：";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(5, 268);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(105, 31);
            this.label12.Text = "輸入數量后按ENTER鍵";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PrintFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(246, 324);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbxRDate);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbxUnit);
            this.Controls.Add(this.tbxQty);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbxLota);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbxGLota);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbxMaktx);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbxMaterial);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbxRemainder);
            this.Controls.Add(this.tbxPrntCount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxBarcode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Name = "PrintFrm";
            this.Text = "標籤複製";
            this.Load += new System.EventHandler(this.PrintFrm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxBarcode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbxPrntCount;
        private System.Windows.Forms.TextBox tbxRemainder;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbxMaterial;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbxMaktx;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbxGLota;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbxLota;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbxQty;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbxUnit;
        private System.Windows.Forms.TextBox tbxRDate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
    }
}