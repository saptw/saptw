﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Intermec.DataCollection;
using System.Data.SQLite;
using System.IO;

namespace PoriteTW
{
    public partial class PDFrm : Form
    {
        private Intermec.DataCollection.BarcodeReader bcr;
        private static string ConnString = "Data Source={0}";
        private static string connString = "";

        // 物料号/批次号/数量/条码ID/日期/序列号/单位
        private string PERSON = "";         //员工编号

        private string MATERIAL = "";       //物料编码
        private string GLOTA = "";          //批次號
        private string QTY = "";            //数量
        private string LABELID = "";        //條碼ID
        private string RDATE = "";          //盘点日期
        private string LABELSN = "";        //序列号
        private string UNIT = "";           //单位
        private string PDQTY = "";          //盘点数量

        private string AREA_NAME = "";      //盘点区域名称
        private string AREA_NO = "";        //盘点区域代码
        private string PDDATE = "";         //盘点写入时间

        private int PDField = -1;           //盘点现场

        private double SCANQTY = 0;         //已经盘点总数

        private string ssname = "";
        private string ssuser = "";
        private string sspasswd = "";
        private string ssdatabase = "";
        private string error = "";

        [DllImport("coredll.dll", EntryPoint = "SendMessage")]
        private static extern int SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);

        private enum Flags
        {
            SND_SYNC = 0x0000,  /* play synchronously (default) */
            SND_ASYNC = 0x0001,  /* play asynchronously */
            SND_NODEFAULT = 0x0002,  /* silence (!default) if sound not found */
            SND_MEMORY = 0x0004,  /* pszSound points to a memory file */
            SND_LOOP = 0x0008,  /* loop the sound until next sndPlaySound */
            SND_NOSTOP = 0x0010,  /* don't stop any currently playing sound */
            SND_NOWAIT = 0x00002000, /* don't wait if the driver is busy */
            SND_ALIAS = 0x00010000, /* name is a registry alias */
            SND_ALIAS_ID = 0x00110000, /* alias is a predefined ID */
            SND_FILENAME = 0x00020000, /* name is file name */
            SND_RESOURCE = 0x00040004  /* name is resource name or atom */
        }

        [DllImport("coredll.dll")]
        private static extern int SetWindowsHookEx(int type, HookKeyProc HookKeyProc, IntPtr hInstance, int m);
        //private static extern int SetWindowsHookEx(int type, HookMouseProc HookMouseProc, IntPtr hInstance, int m);


        [DllImport("coredll.dll")]
        private static extern IntPtr GetModuleHandle(string mod);

        [DllImport("coredll.dll")]
        private static extern int CallNextHookEx(
                HookKeyProc hhk,
                int nCode,
                IntPtr wParam,
                IntPtr lParam
                );

        [DllImport("coredll.dll")]
        private static extern int GetCurrentThreadId();

        [DllImport("coredll.dll", SetLastError = true)]
        private static extern int UnhookWindowsHookEx(int idHook);

        private struct KBDLLHOOKSTRUCT
        {
            public int vkCode;
            public int scanCode;
            public int flags;
            public int time;
            public IntPtr dwExtraInfo;
        }

        const int WH_KEYBOARD_LL = 20;

        public class KeyBoardInfo
        {
            public int vkCode;
            public int scanCode;
            public int flags;
            public int time;
        }

        [DllImport("coredll.dll", EntryPoint = "PlaySound", SetLastError = true)]
        private static extern int WCE_PlaySoundBytes(byte[] szSound, IntPtr hMod, int flags);

        int hHookKey;
        public delegate int HookKeyProc(int code, IntPtr wParam, IntPtr lParam);
        private HookKeyProc hookKeyDeleg;

        //安装钩子
        public void Start()
        {
            if (hHookKey != 0)
            {
                //Unhook the previouse one
                this.Stop();
            }
            hookKeyDeleg = new HookKeyProc(HookKeyProcedure);
            hHookKey = SetWindowsHookEx(WH_KEYBOARD_LL, hookKeyDeleg, GetModuleHandle(null), 0);
            if (hHookKey == 0)
            {
                throw new SystemException("Failed acquiring of the hook.");
            }
        }

        //拆除钩子
        public void Stop()
        {
            UnhookWindowsHookEx(hHookKey);
        }

        private int HookKeyProcedure(int code, IntPtr wParam, IntPtr lParam)
        {
            KBDLLHOOKSTRUCT hookStruct = (KBDLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(KBDLLHOOKSTRUCT));
            if (code < 0)
                return CallNextHookEx(hookKeyDeleg, code, wParam, lParam);
            if (hookStruct.vkCode == 113)
            {
                if (pnlScan.Visible)
                {
                    //tbxHJ.Focus();
                    //tbxHJ.SelectAll();
                }
                return -1; //返回-1表示已经处理了，不再往下传递
            }
            if (hookStruct.vkCode == 0x25)
            {
                //如果捕捉到VK_LEFT按键 
                //......处理......
                return -1;
            }
            // 没处理的键的消息往下传递
            return CallNextHookEx(hookKeyDeleg, code, wParam, lParam);
        }

        public PDFrm()
        {
            InitializeComponent();
            bcr = new BarcodeReader();
            bcr.BarcodeRead += new BarcodeReadEventHandler(bcr_BarcodeRead);
            bcr.ThreadedRead(true);
            bcr.ScannerEnable = true;

        }

        // 清空盤點數據
        private void ClearPDData()
        {
            String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            connString = String.Format(ConnString, progPath + "\\PD.db");

            using (SQLiteWriteLock swlock = new SQLiteWriteLock(connString))
            {
                SQLiteConnection conn = new SQLiteConnection(connString);
                conn.Open();
                SQLiteCommand comm = new SQLiteCommand(conn);
                comm.CommandText = "Delete From Detail";
                comm.ExecuteNonQuery();
                conn.Close();
            }
        }

        // 导出文本文件
        private void ExportToText()
        {
            DataSet ds = null;
            string dir = @"\盤點";

            try
            {
                String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                connString = String.Format(ConnString, progPath + "\\PD.db");

                using (SQLiteWriteLock swlock = new SQLiteWriteLock(connString))
                {
                    SQLiteConnection conn = new SQLiteConnection(connString);
                    conn.Open();
                    SQLiteCommand comm = new SQLiteCommand(conn);
                    //添加新记录
                    comm.CommandText = "SELECT MATERIAL, AREA, LOTA, SUM(QTY) AS QTY, UNIT FROM DETAIL GROUP BY MATERIAL, AREA, LOTA, UNIT";
                    SQLiteDataAdapter adapter = new SQLiteDataAdapter(comm);
                    ds = new DataSet();
                    adapter.Fill(ds, "info");
                    adapter.Dispose();
                    conn.Close();
                }

                StreamWriter swriter = new StreamWriter(dir + @"\" + PERSON + "_" + dtpPD.Value.ToString("yyyyMMdd") + ".CSV", false, Encoding.UTF8);
                swriter.Write("物料號碼");
                swriter.Write("\t");
                swriter.Write("庫存地點");
                swriter.Write("\t");
                swriter.Write("批次號碼");
                swriter.Write("\t");
                swriter.Write("數量");
                swriter.Write("\t");
                swriter.WriteLine("單位");
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    swriter.Write(dr["MATERIAL"].ToString().Trim());
                    swriter.Write("\t");
                    swriter.Write(dr["AREA"].ToString().Trim());
                    swriter.Write("\t");
                    swriter.Write(dr["LOTA"].ToString().Trim());
                    swriter.Write("\t");
                    swriter.Write(dr["QTY"].ToString().Trim());
                    swriter.Write("\t");
                    swriter.WriteLine(dr["UNIT"].ToString().Trim());
                }
                swriter.Close();
                // 清空盤點數據
                ClearPDData();
                // 清空内容
                ClearTextBox();
                MessageBox.Show("盤點數據保存完成！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                tbx2D.Focus();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        // 显示二维码内容
        private void Show2D(string[] split)
        {
            // 物料号/批次号/数量/条码ID/日期/序列号/单位
            // 物料编码
            tbxMATERIAL.Text = split[0].ToString();
            MATERIAL = split[0].ToString();
            // 供应商批次
            tbxGLOTA.Text = split[1].ToString();
            GLOTA = split[1].ToString();
            // 数量
            tbxQTY.Text = split[2].ToString();
            QTY = split[2].ToString();
            // 條碼ID
            tbxLOTA.Text = split[3].ToString();
            LABELID = split[3].ToString();
            //日期
            tbxDate.Text = split[4].ToString();
            RDATE = split[4].ToString();
            // 标签序列号
            LABELSN = split[5].ToString();
            // 單位
            tbxUnit.Text = split[6].ToString();
            UNIT = split[6].ToString();
        }

        // 处理扫描内容
        private void bcr_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            if (pnlMain.Visible)
            {
                tbxPerson.Text = bre.strDataBuffer.ToString().Trim();
                KeyPressEventArgs ek = new KeyPressEventArgs((char)Keys.Enter);
                tbxPerson_KeyPress(tbxPerson, ek);
                return;
            }
            if (pnlScan.Visible)
            {
                if (tbx2D.Focused)
                {
                    tbx2D.Text = bre.strDataBuffer.ToString().Trim();
                    if (bre.Symbology != 24)
                    {
                        MessageBox.Show("當前掃描的條形碼不是有效的QRCODE，請重新掃描正確的QRCODE！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        return;
                    }
                    KeyPressEventArgs ek = new KeyPressEventArgs((char)Keys.Enter);
                    tbx2D_KeyPress(tbx2D, ek);
                    return;
                }
            }
        }

        // 播放声音提示
        public static void PlayAudio(byte[] waveFile)
        {
            WCE_PlaySoundBytes(waveFile, IntPtr.Zero, (int)(Flags.SND_SYNC | Flags.SND_MEMORY));
        }

        // 显示提示信息
        private void ShowHint(string hint)
        {
            if (hint.Equals(""))
            {
                tbxHint.Hide();
                tbxHint.Text = "";
            }
            else
            {
                tbxHint.Show();
                tbxHint.Text = hint;
            }
        }

        // 弹出提示对话框
        private void ShowMessage(string msg, string caption)
        {
            MessageBox.Show(msg, caption);
        }

        // 初始显示界面
        private void ShowPanel(Panel p)
        {
            p.Show();
            p.Location = new Point(0, 0);
            p.Width = Screen.PrimaryScreen.WorkingArea.Width;
            p.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }

        // 清空显示框
        private void ClearTextBox()
        {
            for (int t = 0; t < pnlScan.Controls.Count - 1; t++)
            {
                if (pnlScan.Controls[t].GetType() == typeof(TextBox))
                {
                    ((TextBox)(pnlScan.Controls[t])).Text = "";
                }
            }
        }

        // 窗體加載操作
        private void PDFrm_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            Start();
            pnlMain.Hide();
            pnlScan.Hide();
            ShowPanel(pnlMain);
            tbxPerson.Focus();
            //UDF.GetSERVER(ref ssname, ref ssuser, ref sspasswd, ref ssdatabase);
            //DataSet ds = GetArea();
            //if (ds == null)
            //{
            //    MessageBox.Show("启动失败，"+ error, "提示");
            //    Cursor.Current = Cursors.Default;
            //    this.Close();
            //    return;
            //}
            DataSet ds = UDF.GetPDArea();
            comboBox1.DataSource = ds.Tables[0];
            comboBox1.DisplayMember = "area";
            comboBox1.ValueMember = "code";
            if (ds.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("當前盤點區域尚未維護，請在參數設定里增加盤點區域后再使用！", "訊息");
            }
            Cursor.Current = Cursors.Default;
        }

        // 输入员工编码
        private void tbxPerson_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
                if (tbxPerson.Text.Equals(""))
                {
                    ShowMessage("請輸入或掃描員工編碼!", "訊息");
                    PlayAudio(Properties.Resources.UNFUN);
                    tbxPerson.Focus();
                    return;
                }
                PERSON = tbxPerson.Text.Trim();
                PlayAudio(Properties.Resources.OperateSuccess);
                comboBox1.Focus();
            }
        }

        // 选择盘点区域
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            AREA_NO = comboBox1.SelectedValue.ToString();
        }

        // 退出操作
        private void tbxExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // 窗体关闭
        private void PDFrm_Closing(object sender, CancelEventArgs e)
        {
            bcr.ThreadedRead(false);
            bcr.ScannerEnable = false;
            bcr.Dispose();
            Stop();
        }

        // 下一步操作
        private void btnNext_Click(object sender, EventArgs e)
        {
            if (tbxPerson.Text.Equals(""))
            {
                ShowMessage("請掃描或輸入員工編碼!", "訊息");
                PlayAudio(Properties.Resources.UNFUN);
                tbxPerson.Focus();
                return;
            }
            if (AREA_NO=="")
            {
                ShowMessage("請選擇盤點區域!", "訊息");
                PlayAudio(Properties.Resources.UNFUN);
                comboBox1.Focus();
                return;
            }
            AREA_NAME = comboBox1.Text;
            AREA_NO = comboBox1.SelectedValue.ToString();
            PERSON = tbxPerson.Text;
            lblPerson.Text = "掃描員工編碼：" + PERSON;
            lblPDArea.Text = "盤點區域：" + AREA_NAME + "(" + AREA_NO+ ")";
            PDDATE = dtpPD.Value.ToString("yyyy-MM-dd");
            pnlMain.Hide();
            ShowPanel(pnlScan);
            tbx2D.Focus();
        }

        // 上一步操作
        private void btnBack_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("如果返回上一步操作，系統會清空當前盤點數據，是否繼續？", "訊息", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (result == DialogResult.Yes)
            {
                ClearPDData();
                pnlScan.Hide();
                ClearTextBox();
                ShowPanel(pnlMain);
                tbxPerson.Focus();
                tbxPerson.SelectAll();
            }
        }

        // 处理二维码操作
        private void tbx2D_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
                // 判断二维码是否有效
                if (tbx2D.Text.Equals(""))
                {
                    ShowMessage("请扫描二维码！", "提示");
                    PlayAudio(Properties.Resources.UNFUN);
                    return;
                }
                string[] split = tbx2D.Text.Split('/');
                if (split.Length != 7)
                {
                    tbx2D.Text = "";
                    ShowMessage("当前条码为无效二维码！", "提示");
                    PlayAudio(Properties.Resources.UNFUN);
                    return;
                }
                Show2D(split);
                // 判断物料是否已经有盘点记录
                if (UDF.QueryMaterial(LABELID, LABELSN))
                {
                    MessageBox.Show("當前物料已經盤點完成，請掃描其他QRCODE！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                    PlayAudio(Properties.Resources.UNFUN);
                    tbx2D.SelectAll();
                    return;
                }
                tbxQTY.Focus();
                tbxQTY.SelectAll();
                //UDF.AddDetail(MATERIAL, AREA_NO, GLOTA, QTY, UNIT, PERSON, PDDATE);
                //tbxPDNum.Text = UDF.QueryQTY(PERSON, AREA_NO, PDDATE).ToString();
                //ShowHint("當前物料盤點完成！");
                PlayAudio(Properties.Resources.OperateSuccess);
                //if (QueryLBSN(split[5].ToString()))
                //{
                //    ShowMessage("当前物料标签已经盘点完成，不能重复盘点！", "提示");
                //    PlayAudio(Properties.Resources.UNFUN);
                //    tbx2D.Text = "";
                //    return;
                //}

            }
        }

        // 上传数据，并返回主界面
        private void btnExport_Click(object sender, EventArgs e)
        {
            ExportToText();
            pnlScan.Hide();
            ClearTextBox();
            ShowPanel(pnlMain);
            tbxPerson.Focus();
            tbxPerson.SelectAll();
        }

        // 輸入數量
        private void tbxQTY_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && !Char.IsPunctuation(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;//消除不合适字符  
            }
            else if (Char.IsPunctuation(e.KeyChar))
            {
                if (e.KeyChar != '.' || this.tbxQTY.Text.Length == 0)//小数点  
                {
                    e.Handled = true;
                }
                if (tbxQTY.Text.LastIndexOf('.') != -1)
                {
                    e.Handled = true;
                }
            }  

            if (e.KeyChar == (char)Keys.Enter)
            {
                QTY = tbxQTY.Text;
                tbxQTY.SelectAll();
                btnSave_Click(sender, e);
                //ShowHint("數量修改完成，請點【確認盤點】");
            }
        }

        // 保存盤點記錄
        private void btnSave_Click(object sender, EventArgs e)
        {
            QTY = tbxQTY.Text;
            UDF.AddDetail(MATERIAL, AREA_NO, GLOTA, QTY, UNIT, PERSON, PDDATE, LABELID, LABELSN);
            tbxPDNum.Text = UDF.QueryQTY(PERSON, AREA_NO, PDDATE).ToString();
            ClearTextBox();
            tbx2D.Focus();
            ShowHint("當前物料盤點完成！");
        }
    }
}