﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Runtime.InteropServices;
using Microsoft.WindowsCE.Forms;
using System.Windows;
using ZSDK_API.Printer;
using ZSDK_API.Comm;
using ZSDK_API.ApiException;
using System.Threading;
using System.Net;
using System.IO;

namespace PoriteTW
{
    public partial class ParameterFrm : Form
    {
        public event PrinterConnectionHandler connectionEstablished;
        public event PrinterConnectionHandler connectionClosed;
        private delegate void StatusEventHandler(StatusArguments e);

        private ZebraPrinter printer;
        private ZebraPrinterConnection connection;

        // 標籤中固定文本內容座標
        private int Material_X = 0;
        private int Material_Y = 0;
        private int Maktx_X = 0;
        private int Maktx_Y = 0;
        private int Lota_X = 0;
        private int Lota_Y = 0;
        private int RDate_X = 0;
        private int RDate_Y = 0;
        private int QTY_X = 0;
        private int QTY_Y = 0;
        private int GAL_X = 0;
        private int GAL_Y = 0;
        private int LabelID_X = 0;
        private int LabelID_Y = 0;
        private int Serail_X = 0;
        private int Serial_Y = 0;
        private int QRCODE_X = 0;
        private int QRCODE_Y = 0;
        private int Mark_X = 0;
        private int Mark_Y = 0;
        // 標籤中變量文本內容座標
        private int MaterialValue_X = 0;
        private int MaterialValue_Y = 0;
        private int MaktxValue_X = 0;
        private int MaktxValue_Y = 0;
        private int LotaValue_X = 0;
        private int LotaValue_Y = 0;
        private int RDateValue_X = 0;
        private int RDateValue_Y = 0;
        private int QTYValue_X = 0;
        private int QTYValue_Y = 0;
        private int LabelIDValue_X = 0;
        private int LabelIDValue_Y = 0;
        private int SerailValue_X = 0;
        private int SerialValue_Y = 0;

        private string LBName = "";

        private string btaddress = "";
        private string terminal = "";

        private string pdcode = "";          //盤點編碼
        private static string ConnString = "Data Source={0}";
        private static string connString = "";

        private string ZPL = "";

        [DllImport("coredll.dll", EntryPoint = "SendMessage")]
        private static extern int SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);

        public ParameterFrm()
        {
            InitializeComponent();
            connectionEstablished += new PrinterConnectionHandler(sendLabel);
            connectionClosed += new PrinterConnectionHandler(doDisconnect);
            //InputModeEditor.SetInputMode(tbxServer, InputMode.Default);
            //InputModeEditor.SetInputMode(tbxDatabase, InputMode.AlphaT9);
            //InputModeEditor.SetInputMode(tbxUser, InputMode.AlphaCurrent);
            //InputModeEditor.SetInputMode(tbxPassword, InputMode.Numeric);
        }

        //----------------------------------------------------------------------------
        // 消息显示处理
        public void SetStatus(String message, Color color)
        {
            this.lblHint.Text = message;
            this.lblHint.BackColor = color;
        }

        public void updateGuiFromWorkerThread(String message, Color color)
        {
            Invoke(new StatusEventHandler(UpdateUI), new StatusArguments(message, color));
        }

        private void UpdateUI(StatusArguments e)
        {
            SetStatus("狀態: " + e.message, e.color);
        }

        private class StatusArguments : System.EventArgs
        {
            public String message;
            public Color color;

            public StatusArguments(String message, Color color)
            {
                this.message = message;
                this.color = color;
            }
        }
        //------------------------------------------------------------------------------

        // 打印通讯相关函数
        public String BluetoothMacAddress
        {
            get
            {
                return tbxBTAddress.Text;
            }
            set
            {
                tbxBTAddress.Text = value;
            }
        }

        public void connect()
        {
            connectBluetooth(BluetoothMacAddress);
        }

        private String macAddress;

        private void connectBluetooth(String macAddress)
        {
            this.macAddress = macAddress.Trim();
            Thread t = new Thread(doConnectBT);
            t.Start();
        }

        private void doConnectBT()
        {
            if (this.macAddress.Length != 12)
            {
                updateGuiFromWorkerThread("無效的列印機 MAC 地址", Color.Red);
                disconnect();
            }
            else
            {
                updateGuiFromWorkerThread("列印機連接中...請稍候...", Color.Goldenrod);
                try
                {
                    connection = new BluetoothPrinterConnection(this.macAddress);
                    threadedConnect(this.macAddress);
                }
                catch (ZebraException)
                {
                    updateGuiFromWorkerThread("列印機通訊失敗，請檢察PDA藍牙是否打開！", Color.Red);
                    connectionClosed();
                }
            }
        }

        public void disconnect()
        {
            Thread t = new Thread(doDisconnect);
            t.Start();
        }

        private void doDisconnect()
        {
            try
            {
                if (connection != null && connection.IsConnected())
                {
                    //updateGuiFromWorkerThread("断开连接！", Color.SkyBlue);
                    connection.Close();
                    Thread.Sleep(1000);
                    updateGuiFromWorkerThread("列印完成，斷開連接！", Color.SkyBlue);
                    connection = null;
                }
            }
            catch (ZebraException)
            {
                updateGuiFromWorkerThread("通訊錯誤，藍牙連接失敗！", Color.Red);
            }
        }

        // 连接打印机线程
        private void threadedConnect(String addressName)
        {
            try
            {
                connection.Open();
                Thread.Sleep(1000);
            }
            catch (ZebraPrinterConnectionException)
            {
                updateGuiFromWorkerThread("無法連接到列印機，請檢測列印機！", Color.Red);
                disconnect();
            }
            catch (ZebraGeneralException e)
            {
                updateGuiFromWorkerThread(e.Message, Color.Red);
                disconnect();
            }
            catch (Exception)
            {
                updateGuiFromWorkerThread("列印機通訊錯誤！", Color.Red);
                disconnect();
            }

            printer = null;
            if (connection != null && connection.IsConnected())
            {
                try
                {
                    printer = ZebraPrinterFactory.GetInstance(connection);
                    PrinterLanguage pl = printer.GetPrinterControlLanguage();
                    updateGuiFromWorkerThread("準備打印中......", Color.Goldenrod);
                    Thread.Sleep(1000);
                    //updateGuiFromWorkerThread("Connected to " + addressName, Color.Green);
                    //Thread.Sleep(1000);
                    this.connectionEstablished();
                }
                catch (ZebraPrinterConnectionException)
                {
                    updateGuiFromWorkerThread("列印機連接失敗！", Color.Red);
                    printer = null;
                    Thread.Sleep(1000);
                    disconnect();
                }
                catch (ZebraPrinterLanguageUnknownException)
                {
                    updateGuiFromWorkerThread("無效的列印指令！", Color.Red);
                    printer = null;
                    Thread.Sleep(1000);
                    disconnect();
                }
            }
        }

        public ZebraPrinterConnection getConnection()
        {
            return connection;
        }

        public ZebraPrinter getPrinter()
        {
            return printer;
        }

        private byte[] getConfigLabel(string zpl)
        {
            return Encoding.GetEncoding("utf-8").GetBytes(zpl);
        }

        public void sendLabel()
        {
            ZebraPrinter printer = getPrinter();
            if (printer != null)
            {
                byte[] configLabel = getConfigLabel(ZPL);
                getConnection().Write(configLabel);
                updateGuiFromWorkerThread("打印中...", Color.SkyBlue);
                Thread.Sleep(1000);
            }
            doDisconnect();
        }

        // 获取当前设备IP地址
        private string GetIPAddress()
        {
            IPHostEntry ipEntry = Dns.GetHostByName(Dns.GetHostName());

            if (ipEntry.AddressList.Length == 1)
            {
                return ipEntry.AddressList[0].ToString();
            }
            else
                return ipEntry.AddressList[1].ToString();
        }

        private void ParameterFrm_Load(object sender, EventArgs e)
        {
            //UDF.GetSERVER(ref ssname, ref ssuser, ref sspasswd, ref ssdatabase);
            UDF.GetSAPWS(ref UDF.SAP_SERVER, ref UDF.SAP_PORT, ref UDF.SAP_USER, ref UDF.SAP_PASS, ref UDF.SAP_CLIENT);
            UDF.GetBluetooth(ref btaddress, ref terminal);
            UDF.LoadCoord(listView2);

            tbxServer.Text = UDF.SAP_SERVER;
            tbxUser.Text = UDF.SAP_USER;
            tbxPassword.Text = UDF.SAP_PASS;
            tbxPort.Text = UDF.SAP_PORT;
            tbxClient.Text = UDF.SAP_CLIENT;
            tbxBTAddress.Text = btaddress;
            lblIPAddress.Text = "当前设备IP：" + GetIPAddress();
        }

        // 保存WEB SERVER參數設置
        private void btnSQLSave_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(tbxServer.Text))
            {
                MessageBox.Show("服務器IP地址不能為空！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return;
            }
            UDF.SAP_SERVER = tbxServer.Text;
            if (String.IsNullOrEmpty(tbxPort.Text))
            {
                MessageBox.Show("服務器端口不能為空！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return;
            }
            UDF.SAP_PORT = tbxPort.Text;
            if (String.IsNullOrEmpty(tbxUser.Text))
            {
                MessageBox.Show("SAP用戶名不能為空！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return;
            }
            UDF.SAP_USER = tbxUser.Text;
            if (String.IsNullOrEmpty(tbxPassword.Text))
            {
                MessageBox.Show("SAP訪問密碼不能為空！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return;
            }
            UDF.SAP_PASS = tbxPassword.Text;
            if (String.IsNullOrEmpty(tbxClient.Text))
            {
                MessageBox.Show("SAP Client不能為空！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return;
            }
            UDF.SAP_CLIENT = tbxClient.Text;
            UDF.SetSAPWS(UDF.SAP_SERVER, UDF.SAP_PORT, UDF.SAP_USER, UDF.SAP_PASS, UDF.SAP_CLIENT);
            MessageBox.Show("SAP WEB SERVICE 參數設定完成！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }

        private void ShowArea()
        {
            DataSet ds = UDF.GetPDArea();
            listView1.Items.Clear();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = dr[0].ToString().Trim();
                lvi.SubItems.Add(dr[1].ToString().Trim());
                listView1.Items.Add(lvi);
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabControl1.SelectedIndex)
            {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    ShowArea();
                    break;
                case 3:
                    Close();
                    break;
            }
            //if (tabControl1.SelectedIndex == 3)
            //{
            //    this.Close();
            //}
        }

        private void tbxServer_GotFocus(object sender, EventArgs e)
        {
            SendMessage(tbxServer.Handle, 0x287, 0x30, 0x0);
        }

        // 保存藍牙通訊地址
        private void btnBTSave_Click(object sender, EventArgs e)
        {
            if (tbxBTAddress.Text.Equals(""))
            {
                MessageBox.Show("請輸入列印機的藍牙地址！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                return;
            }
            if (tbxBTAddress.Text.Length != 12)
            {
                MessageBox.Show("請輸入有效的藍牙地址！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                return;
            }
            UDF.SetBluetooth(tbxBTAddress.Text, "");
            MessageBox.Show("列印機藍牙地址輸入完成。", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }

        private void tbxDatabase_GotFocus(object sender, EventArgs e)
        {
            SendMessage(tbxPort.Handle, 0x287, 0x30, 0x0);
        }

        private void tbxUser_GotFocus(object sender, EventArgs e)
        {
            SendMessage(tbxUser.Handle, 0x287, 0x30, 0x0);
        }

        private void tbxBTAddress_GotFocus(object sender, EventArgs e)
        {
            SendMessage(tbxBTAddress.Handle, 0x287, 0x30, 0x0);
        }

        // 打印測試
        private void button1_Click(object sender, EventArgs e)
        {
            DataSet ds = UDF.GetLabelCoord();

            Material_X = Convert.ToInt32(ds.Tables[0].Rows[0][3].ToString());
            Material_Y = Convert.ToInt32(ds.Tables[0].Rows[0][4].ToString());
            Maktx_X = Convert.ToInt32(ds.Tables[0].Rows[1][3].ToString());
            Maktx_Y = Convert.ToInt32(ds.Tables[0].Rows[1][4].ToString());
            Lota_X = Convert.ToInt32(ds.Tables[0].Rows[2][3].ToString());
            Lota_Y = Convert.ToInt32(ds.Tables[0].Rows[2][4].ToString());
            RDate_X = Convert.ToInt32(ds.Tables[0].Rows[3][3].ToString());
            RDate_Y = Convert.ToInt32(ds.Tables[0].Rows[3][4].ToString());
            QTY_X = Convert.ToInt32(ds.Tables[0].Rows[4][3].ToString());
            QTY_Y = Convert.ToInt32(ds.Tables[0].Rows[4][4].ToString());
            GAL_X = Convert.ToInt32(ds.Tables[0].Rows[5][3].ToString());
            GAL_Y = Convert.ToInt32(ds.Tables[0].Rows[5][4].ToString());
            LabelID_X = Convert.ToInt32(ds.Tables[0].Rows[6][3].ToString());
            LabelID_Y = Convert.ToInt32(ds.Tables[0].Rows[6][4].ToString());
            Serail_X = Convert.ToInt32(ds.Tables[0].Rows[7][3].ToString());
            Serial_Y = Convert.ToInt32(ds.Tables[0].Rows[7][4].ToString());
            QRCODE_X = Convert.ToInt32(ds.Tables[0].Rows[8][3].ToString());
            QRCODE_Y = Convert.ToInt32(ds.Tables[0].Rows[8][4].ToString());
            Mark_X = Convert.ToInt32(ds.Tables[0].Rows[9][3].ToString());
            Mark_Y = Convert.ToInt32(ds.Tables[0].Rows[9][4].ToString());

            MaterialValue_X = Convert.ToInt32(ds.Tables[0].Rows[10][3].ToString());
            MaterialValue_Y = Convert.ToInt32(ds.Tables[0].Rows[10][4].ToString());
            MaktxValue_X = Convert.ToInt32(ds.Tables[0].Rows[11][3].ToString());
            MaktxValue_Y = Convert.ToInt32(ds.Tables[0].Rows[11][4].ToString());
            LotaValue_X = Convert.ToInt32(ds.Tables[0].Rows[12][3].ToString());
            LotaValue_Y = Convert.ToInt32(ds.Tables[0].Rows[12][4].ToString());
            RDateValue_X = Convert.ToInt32(ds.Tables[0].Rows[13][3].ToString());
            RDateValue_Y = Convert.ToInt32(ds.Tables[0].Rows[13][4].ToString());
            QTYValue_X = Convert.ToInt32(ds.Tables[0].Rows[14][3].ToString());
            QTYValue_Y = Convert.ToInt32(ds.Tables[0].Rows[14][4].ToString());
            LabelIDValue_X = Convert.ToInt32(ds.Tables[0].Rows[15][3].ToString());
            LabelIDValue_Y = Convert.ToInt32(ds.Tables[0].Rows[15][4].ToString());
            SerailValue_X = Convert.ToInt32(ds.Tables[0].Rows[16][3].ToString());
            SerialValue_Y = Convert.ToInt32(ds.Tables[0].Rows[16][4].ToString());

            ZPL = "";
            ZPL += "^XA";
            ZPL += "^LT0^LH0,0^MNY";
            ZPL += "^CW1,E:ANMDS.TTF";
            ZPL += "^CI28";
            ZPL += "^FO" + QRCODE_X.ToString() + "," + QRCODE_Y.ToString();
            ZPL += "^BQN,2,9^FDMA,ZL21401-D5A/E2123001/50.000/E000000001/201705016/00/GAL^FS";
            ZPL += "^FT" + Material_X.ToString() + "," + Material_Y.ToString() + "^A1N,40,40^FD編碼:^FS";
            ZPL += "^FT" + MaterialValue_X + "," + MaterialValue_Y + "^A1N,40,40^FDZL21401-D5A^FS";

            ZPL += "^FT" + Maktx_X.ToString() + "," + Maktx_Y.ToString() + "^A1N,40,40^FD名稱: " + "" + "^FS";
            ZPL += "^FT" + MaktxValue_X.ToString() + "," + MaktxValue_Y.ToString() + "^A1N,24,24^FD旋钮^FS";

            ZPL += "^FT" + Lota_X.ToString() + "," + Lota_Y.ToString() + "^A1N,40,40^FD供應商批次:^FS";
            ZPL += "^FT" + LotaValue_X.ToString() + "," + LotaValue_Y.ToString() + "^A1N,40,40^FDE2123001^FS";

            ZPL += "^FT" + RDate_X.ToString() + "," + RDate_Y.ToString() + "^A1N,40,40^FD入庫月份:^FS";
            ZPL += "^FT" + RDateValue_X.ToString() + "," + RDateValue_Y.ToString() + "^A1N,40,40^FD2017/05/16^FS";

            ZPL += "^FT" + QTY_X.ToString() + "," + QTY_Y.ToString() + "^A1N,40,40^FD數量:^FS";
            ZPL += "^FT" + QTYValue_X.ToString() + "," + QTYValue_Y.ToString() + "^A1N,40,40^FD50.000^FS";

            ZPL += "^FT" + GAL_X.ToString() + "," + GAL_Y.ToString() + "^A1N,40,40^FDGAL^FS";

            ZPL += "^FT" + LabelID_X.ToString() + "," + LabelID_Y.ToString() + "^A1N,40,40^FDLABEL ID:^FS";
            ZPL += "^FT" + LabelIDValue_X.ToString() + "," + LabelIDValue_Y.ToString() + "^A1N,40,40^FDE000000001^FS";

            ZPL += "^FT" + Serail_X.ToString() + "," + Serial_Y.ToString() + "^A1N,40,40^FD序列號: ^FS";
            ZPL += "^FT" + SerailValue_X.ToString() + "," + SerialValue_Y.ToString() + "^A1N,40,40^FD00^FS";

            ZPL += "^FT" + Mark_X.ToString() + "," + Mark_Y.ToString() + "^A1N,40,40^FD999^FS";

            ZPL += "^XZ";
            //MessageBox.Show("^FT" + Mark_X.ToString() + "," + Mark_Y.ToString() + "^A1N,40,40^FD999^FS");
            //StreamWriter swrite = new StreamWriter(@"\ZPL.TXT");
            //swrite.Write(ZPL);
            //swrite.Close();

            //ZPL += "";
            //ZPL += "^XA";
            //ZPL += "^LT0^LH0,0";
            //ZPL += "^CW1,E:MSSUNG24.FNT";
            //ZPL += "^CI28";
            //ZPL += "^FO82,486^BQN,2,9^FDMA,ZL21401-D5A/E2123001/50.000/E000000001/201705016/00/GAL^FS";
            //ZPL += "^FT42,91^A1N,32,32^FD編碼:^FS";
            //ZPL += "^FT290,91^A1N,32,32^FDZL21401-D5A^FS";
            //ZPL += "^FT42,154^A1N,32,32^FD名稱: ^FS";
            //ZPL += "^FO290,151^A1N,32,32^FDCRC 3.36^FS";
            //ZPL += "^FO42,144^A1N,32,32^FD供應商批次:^FS";
            //ZPL += "^FO290,212^A1N,32,32^FDE2123001^FS";
            //ZPL += "^FO42,279^A1N,32,32^FD入庫月份:^FS";
            //ZPL += "^FO290,274^A1N,32,32^FD2017/05/16^FS";
            //ZPL += "^FO42,342^A1N,32,32^FD數量:^FS";
            //ZPL += "^FO290,335^A1N,32,32^FD50.000^FS";
            //ZPL += "^FO441,335^A1N,32,32^FDGAL^FS";
            //ZPL += "^FO42,404^A1N,32,32^FDLABEL ID:^FS";
            //ZPL += "^FO290,396^A1N,32,32^FDE000000001^FS";
            //ZPL += "^FO42,467^A1N,32,32^FD序列號: ^FS";
            //ZPL += "^FO290,458^A1N,32,32^FD00^FS";
            //ZPL += "^FT417,633^A1N,32,32^FD999^FS";
            //ZPL += "^XZ";
            connect();
        }

        // 刪除盤點區域
        private void btnDeleteArea_Click(object sender, EventArgs e)
        {
            if (!pdcode.Equals(""))
            {
                DialogResult dr = MessageBox.Show("是否刪除訪盤點區域？", "詢問", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dr == DialogResult.No)
                {
                    return;
                }
                UDF.DeletePDArea(pdcode);
                ShowArea();
                MessageBox.Show("盤點區域刪除完成！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            }
            else
            {
                MessageBox.Show("請選擇要刪除的盤點區域！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            }
        }

        // 選擇一條記錄
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((listView1.SelectedIndices != null) && (listView1.SelectedIndices.Count > 0))
            {
                ListView.SelectedIndexCollection c = listView1.SelectedIndices;
                pdcode = listView1.Items[c[0]].SubItems[1].Text;
            }
            else
            {
                pdcode = String.Empty;
            }
        }

        // 增加一条盤點區域记录
        private void btnSaveArea_Click(object sender, EventArgs e)
        {
            if (tbxPDArea.Text.Equals(""))
            {
                MessageBox.Show("盤點區域不能為空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return;
            }
            if (tbxPDCode.Text.Equals(""))
            {
                MessageBox.Show("盤點編碼不能為空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return;
            }
            UDF.AddPDArea(tbxPDArea.Text, tbxPDCode.Text);
            ShowArea();
            MessageBox.Show("盤點區域增加完成！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }

        private void tbxPDArea_GotFocus(object sender, EventArgs e)
        {
            inputPanel1.Enabled = true;
        }

        private void tbxPDArea_LostFocus(object sender, EventArgs e)
        {
            inputPanel1.Enabled = false;
        }

        private void tbxPDCode_GotFocus(object sender, EventArgs e)
        {
            inputPanel1.Enabled = true;
        }

        private void tbxPDCode_LostFocus(object sender, EventArgs e)
        {
            inputPanel1.Enabled = false;
        }

        // WS 測試
        private void btnWebTest_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                System.Net.NetworkCredential c = new System.Net.NetworkCredential(tbxUser.Text, tbxPassword.Text);
                SAPWR.Z_RAW_MATERIAL_LABEL_COPY func = new PoriteTW.SAPWR.Z_RAW_MATERIAL_LABEL_COPY();
                func.Credentials = c;
                func.SoapVersion = System.Web.Services.Protocols.SoapProtocolVersion.Soap12;
                func.Url = "http://" + tbxServer.Text + ":" + tbxPort.Text + "/sap/bc/srt/rfc/sap/z_raw_material_label_copy/" + tbxClient.Text + "/z_raw_material_label_copy/z_raw_material_label_copy";
                //定义接口所需要参数
                SAPWR.ZRawMaterialLabelCopy copy = new PoriteTW.SAPWR.ZRawMaterialLabelCopy();
                copy.Material = "ZL01107-P2A";
                copy.Plant = "PE01";
                copy.ZbatchNo = "20170118";
                copy.Zlabelid = "E000000935";
                SAPWR.ZRawMaterialLabelCopyResponse resp = func.ZRawMaterialLabelCopy(copy);
                string result = "MaterialTx:" + resp.MaterialTx;
                result += "\n\rOmaterial:" + resp.Omaterial;
                result += "\n\rVersio:" + resp.Versio;
                result += "\n\rZreturn:" + resp.Zreturn;
                MessageBox.Show("測試結果:\n\r" + result, "訊息", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            }
            catch (WebException wex)
            {
                MessageBox.Show("異常訊息:\n\r" + wex.Message +"\n\r請檢查網路是否正常！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            Cursor.Current = Cursors.Default;
        }

        // 選擇標籤座標值
        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((listView2.SelectedIndices != null) && (listView2.SelectedIndices.Count > 0))
            {
                ListView.SelectedIndexCollection c = listView2.SelectedIndices;
                LBName = listView2.Items[c[0]].Text;
                tbxXCoord.Text = listView2.Items[c[0]].SubItems[1].Text;
                tbxYCoord.Text = listView2.Items[c[0]].SubItems[2].Text;
            }
            else
            {
                tbxXCoord.Text = String.Empty;
                tbxYCoord.Text = String.Empty;
            }
        }

        private void btnSaveCoord_Click(object sender, EventArgs e)
        {
            if (tbxXCoord.Text == "")
            {
                return;
            }
            if (tbxYCoord.Text == "")
            {
                return;
            }
            UDF.SetLabelCoord(Convert.ToInt32(tbxXCoord.Text), Convert.ToInt32(tbxYCoord.Text), LBName);
            UDF.LoadCoord(listView2);
            MessageBox.Show(LBName + "座標修改完成", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }

        // 清空盤點數據
        private void ClearPDData()
        {
            String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            connString = String.Format(ConnString, progPath + "\\PD.db");

            using (SQLiteWriteLock swlock = new SQLiteWriteLock(connString))
            {
                SQLiteConnection conn = new SQLiteConnection(connString);
                conn.Open();
                SQLiteCommand comm = new SQLiteCommand(conn);
                comm.CommandText = "Delete From PRODUCT";
                comm.ExecuteNonQuery();
                conn.Close();
            }
        }

        // 清空盤點數據
        private void btnInitPD_Click(object sender, EventArgs e)
        {
            string sql = String.Format("SELECT *FROM PRODUCT");
            DataSet ds = UDF.SelectProductPD(sql);
            if (ds.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("當前盤點數據已經清空，無需再次清空！", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return;
            }
            DialogResult dressult = MessageBox.Show("是否清空成品盤點數據？", "詢問", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dressult == DialogResult.Yes)
            {
                ClearPDData();
            }
        }
    }
}