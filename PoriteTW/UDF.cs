﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SQLite;
using System.Windows;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Microsoft.Win32;
using System.Drawing;

namespace PoriteTW
{
    class UDF
    {
        private static string ConnString = "Data Source={0}";
        private static string connString = "";

        public static string SAP_SERVER = "10.5.20.14";
        public static string SAP_PORT = "8000";
        public static string SAP_USER = "CNRFC";
        public static string SAP_PASS = "porite01";
        public static string SAP_CLIENT = "411";

        /*设置软件显示全屏*/
        public static bool SetFullScreen(bool fullscreen, ref Rectangle rectOld)
        {
            int Hwnd = 0;
            Hwnd = Win32.FindWindow("HHTaskBar", null);
            if (Hwnd == 0) return false;
            if (fullscreen)
            {
                Win32.ShowWindow((IntPtr)Hwnd, Win32.SW_HIDE);
                Rectangle rectFull = Screen.PrimaryScreen.Bounds;
                Win32.SystemParametersInfo(Win32.SPI_GETWORKAREA, 0, ref rectOld, Win32.SPIF_UPDATEINIFILE);//get
                Win32.SystemParametersInfo(Win32.SPI_SETWORKAREA, 0, ref rectFull, Win32.SPIF_UPDATEINIFILE);//set
            }
            else
            {
                Win32.ShowWindow((IntPtr)Hwnd, Win32.SW_SHOW);
                Win32.SystemParametersInfo(Win32.SPI_SETWORKAREA, 0, ref rectOld, Win32.SPIF_UPDATEINIFILE);
            }
            return true;
        }

        /*设置SAP参数信息*/
        public static void SetSAPWS(string ipadr, string port, string user, string pass, string client)
        {
            bool b = false;
            try
            {
                String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                SQLiteConnection conn = new SQLiteConnection(String.Format(ConnString, progPath + "\\param.db"));
                conn.Open();
                SQLiteCommand comm = new SQLiteCommand(conn);
                //查询是否有记录存在
                comm.CommandText = "SELECT *FROM SAPWS";
                SQLiteDataReader sdReader = comm.ExecuteReader();
                if (sdReader.Read())
                {
                    b = true;
                }
                sdReader.Close();
                if (!b)
                {
                    //添加新记录
                    comm.CommandText = String.Format("INSERT INTO SAPWS VALUES('{0}', '{1}', '{2}', '{3}', '{4}')", ipadr, port, user,pass,client);
                    comm.ExecuteNonQuery();
                }
                else
                {
                    //修改新记录
                    comm.CommandText = String.Format("UPDATE SAPWS SET ipaddress='{0}',port='{1}', user='{2}', passwd='{3}', client='{4}'", ipadr, port, user, pass, client);
                    comm.ExecuteNonQuery();
                }
                conn.Close();
                //MessageBox.Show("数据完成!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*获取SAP参数信息*/
        public static void GetSAPWS(ref string ipard,ref string port,ref string user,ref string pass, ref string client)
        {
            try
            {
                String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                SQLiteConnection sqlconn = new SQLiteConnection(String.Format(ConnString, progPath + "\\param.db"));
                sqlconn.Open();
                SQLiteCommand sqlcom = new SQLiteCommand(sqlconn);
                sqlcom.CommandText = "SELECT *FROM SAPWS";
                SQLiteDataReader sdReader = sqlcom.ExecuteReader();
                if (sdReader.Read())
                {
                    ipard = sdReader[0].ToString().Trim();
                    port = sdReader[1].ToString().Trim();
                    user = sdReader[2].ToString().Trim();
                    pass = sdReader[3].ToString().Trim();
                    client = sdReader[4].ToString().Trim();
                }
                sdReader.Close();
                sqlconn.Close();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*设置蓝牙打印机参数信息*/
        public static void SetBluetooth(string btaddress, string terminal)
        {
            bool b = false;
            try
            {
                String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                SQLiteConnection conn = new SQLiteConnection(String.Format(ConnString, progPath + "\\param.db"));
                conn.Open();
                SQLiteCommand comm = new SQLiteCommand(conn);
                //查询是否有记录存在
                comm.CommandText = "SELECT *FROM BLUETOOTH";
                SQLiteDataReader sdReader = comm.ExecuteReader();
                if (sdReader.Read())
                {
                    b = true;
                }
                sdReader.Close();
                if (!b)
                {
                    //添加新记录
                    comm.CommandText = "INSERT INTO BLUETOOTH(BTADDRESS, TERMINAL)" +
                                       " VALUES(@BTADDRESS, @TERMINAL)";
                    comm.Parameters.AddWithValue("@BTADDRESS", btaddress);
                    comm.Parameters.AddWithValue("@TERMINAL", terminal);
                    comm.ExecuteNonQuery();
                }
                else
                {
                    //修改新记录
                    comm.CommandText = "UPDATE BLUETOOTH SET BTADDRESS=@BTADDRESS,TERMINAL=@TERMINAL";
                    comm.Parameters.AddWithValue("@BTADDRESS", btaddress);
                    comm.Parameters.AddWithValue("@TERMINAL", terminal);
                    comm.ExecuteNonQuery();
                }
                conn.Close();
                //MessageBox.Show("数据完成!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*获取蓝牙打印机参数信息*/
        public static void GetBluetooth(ref string btaddress, ref string terminal)
        {
            try
            {
                String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                SQLiteConnection sqlconn = new SQLiteConnection(String.Format(ConnString, progPath + "\\param.db"));
                sqlconn.Open();
                SQLiteCommand sqlcom = new SQLiteCommand(sqlconn);
                sqlcom.CommandText = "SELECT *FROM BLUETOOTH";
                SQLiteDataReader sdReader = sqlcom.ExecuteReader();
                if (sdReader.Read())
                {
                    btaddress = sdReader[0].ToString().Trim();
                    terminal = sdReader[1].ToString().Trim();
                }
                sdReader.Close();
                sqlconn.Close();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*获取盘点区域信息*/
        public static DataSet GetPDArea()
        {
            DataSet dsResult = null;

            try
            {
                String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                SQLiteConnection sqlconn = new SQLiteConnection(String.Format(ConnString, progPath + "\\param.db"));
                sqlconn.Open();
                SQLiteCommand sqlcom = new SQLiteCommand(sqlconn);
                sqlcom.CommandText = "SELECT *FROM PDAREA";
                SQLiteDataAdapter Adapter = new SQLiteDataAdapter(sqlcom);
                dsResult = new DataSet();
                Adapter.Fill(dsResult, "PDAREA");
                Adapter.Dispose();
                sqlconn.Close();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }

            return dsResult;
        }

        /*刪除盤點區域*/
        public static void DeletePDArea(string pdcode)
        {
            try
            {
                String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                SQLiteConnection sqlconn = new SQLiteConnection(String.Format(ConnString, progPath + "\\param.db"));
                sqlconn.Open();
                SQLiteCommand comm = new SQLiteCommand(sqlconn);
                comm.CommandText = String.Format("DELETE FROM PDAREA WHERE code='{0}'", pdcode);
                comm.ExecuteNonQuery();
                sqlconn.Close();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*查詢是否已經有盤點區域慧*/
        public static bool QueryPDArea(string pdcode)
        {
            bool bResult = false;

            try
            {
                String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                SQLiteConnection sqlconn = new SQLiteConnection(String.Format(ConnString, progPath + "\\param.db"));
                sqlconn.Open();
                SQLiteCommand comm = new SQLiteCommand(sqlconn);
                comm.CommandText = String.Format("SELECT *FROM PDAREA WHERE code='{0}'", pdcode);
                SQLiteDataReader dreader = comm.ExecuteReader();
                if (dreader.Read())
                {
                    bResult = true;
                }
                sqlconn.Close();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }

            return bResult;
        }

        /*增加新的盤點區域*/
        public static void AddPDArea(string area, string code)
        {
            try
            {
                String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                SQLiteConnection sqlconn = new SQLiteConnection(String.Format(ConnString, progPath + "\\param.db"));
                sqlconn.Open();
                SQLiteCommand comm = new SQLiteCommand(sqlconn);
                comm.CommandText = String.Format("INSERT INTO PDAREA VALUES('{0}', '{1}')", area, code);
                comm.ExecuteNonQuery();
                sqlconn.Close();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*增加盤點主記錄*/
        public static int AddMaster(string person, string area, string date)
        {
            String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            connString = String.Format(ConnString, progPath + "\\pd.db");
            int result = 0;

            using (SQLiteWriteLock swlock = new SQLiteWriteLock(connString))
            {
                SQLiteConnection conn = new SQLiteConnection(connString);
                conn.Open();
                SQLiteCommand comm = new SQLiteCommand(conn);
                comm.CommandText = String.Format("SELECT *FROM Master WHERE person='{0}' AND area='{1}'", person, area);
                SQLiteDataReader dreader = comm.ExecuteReader();
                if (!dreader.Read())
                {
                    dreader.Close();
                    //记录一条新订单数据
                    comm.CommandText = String.Format("INSERT INTO Master VALUES('{0}', '{1}', '{2}')", person, area, date);
                    result = comm.ExecuteNonQuery();
                }
                else
                {
                    dreader.Close();
                }
                conn.Close();
            }
            return result;
        }

        /*增加盤點明細記錄*/
        public static int AddDetail(string material, string area, string lota, string qty, string unit, string person, string pdate, string labelid, string labelsn)
        {
            String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            connString = String.Format(ConnString, progPath + "\\pd.db");
            int result = 0;

            using (SQLiteWriteLock swlock = new SQLiteWriteLock(connString))
            {
                SQLiteConnection conn = new SQLiteConnection(connString);
                conn.Open();
                SQLiteCommand comm = new SQLiteCommand(conn);
                comm.CommandText = String.Format("SELECT *FROM Detail WHERE labelid='{0}' AND labelsn='{1}' ", labelid, labelsn);
                SQLiteDataReader dreader = comm.ExecuteReader();
                if (!dreader.Read())
                {
                    dreader.Close();
                    //记录一条新订单数据
                    comm.CommandText = String.Format("INSERT INTO Detail VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}','{7}','{8}')", material, area, lota, qty, unit, person, pdate, labelid, labelsn);
                    result = comm.ExecuteNonQuery();
                }
                else
                {
                    dreader.Close();
                }
                conn.Close();
            }
            return result;
        }

        /*查詢已盤點總數量*/
        public static int QueryQTY(string person, string area, string pdate)
        {
            String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            connString = String.Format(ConnString, progPath + "\\pd.db");
            int result = 0;

            using (SQLiteWriteLock swlock = new SQLiteWriteLock(connString))
            {
                SQLiteConnection conn = new SQLiteConnection(connString);
                conn.Open();
                SQLiteCommand comm = new SQLiteCommand(conn);
                comm.CommandText = String.Format("SELECT COUNT(*) FROM Detail WHERE person='{0}' AND area='{1}' AND pdate='{2}'", person, area, pdate);
                SQLiteDataReader dreader = comm.ExecuteReader();
                if (dreader.Read())
                {
                    result = Convert.ToInt32(dreader[0].ToString());
                }
                conn.Close();
            }
            return result;
        }

        /*判斷當前物料是否已經盤點*/
        public static bool QueryMaterial(string labelid,string labelsn)
        {
            String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            connString = String.Format(ConnString, progPath + "\\pd.db");
            bool result = false;

            using (SQLiteWriteLock swlock = new SQLiteWriteLock(connString))
            {
                SQLiteConnection conn = new SQLiteConnection(connString);
                conn.Open();
                SQLiteCommand comm = new SQLiteCommand(conn);
                comm.CommandText = String.Format("SELECT * FROM Detail WHERE labelid='{0}' AND labelsn='{1}'", labelid, labelsn);
                SQLiteDataReader dreader = comm.ExecuteReader();
                if (dreader.Read())
                {
                    result = true;
                } 
                conn.Close();
            }
            return result;
        }

        /*加載座標信息*/
        public static void LoadCoord(ListView lv)
        {
            lv.Items.Clear();
            DataSet ds = new DataSet("label");
            try
            {
                String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                connString = String.Format(ConnString, progPath + "\\param.db");

                using (SQLiteWriteLock swlock = new SQLiteWriteLock(connString))
                {
                    SQLiteConnection sqlconn = new SQLiteConnection(connString);
                    sqlconn.Open();
                    SQLiteCommand comm = new SQLiteCommand(sqlconn);
                    comm.CommandText = String.Format("SELECT *FROM Label ORDER BY ID");
                    SQLiteDataAdapter Adapter = new SQLiteDataAdapter(comm);
                    Adapter.Fill(ds);

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ListViewItem lvi = new ListViewItem();
                        lvi.Text = dr[1].ToString();
                        lvi.SubItems.Add(dr[3].ToString());
                        lvi.SubItems.Add(dr[4].ToString());
                        lv.Items.Add(lvi);
                    }
                    ds.Clear();
                    ds.Dispose();
                    sqlconn.Close();
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*獲取所有打印內容的座標*/
        public static DataSet GetLabelCoord()
        {
            DataSet ds = new DataSet("label");
            try
            {
                String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                connString = String.Format(ConnString, progPath + "\\param.db");

                using (SQLiteWriteLock swlock = new SQLiteWriteLock(connString))
                {
                    SQLiteConnection sqlconn = new SQLiteConnection(connString);
                    sqlconn.Open();
                    SQLiteCommand comm = new SQLiteCommand(sqlconn);
                    comm.CommandText = String.Format("SELECT *FROM Label ORDER BY ID");
                    SQLiteDataAdapter Adapter = new SQLiteDataAdapter(comm);
                    Adapter.Fill(ds);
                    sqlconn.Close();
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
            return ds;
        }

        /*修改座標*/
        public static void SetLabelCoord(int XCoord, int YCoord, string lbname)
        {
            try
            {
                String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                connString = String.Format(ConnString, progPath + "\\param.db");

                using (SQLiteWriteLock swlock = new SQLiteWriteLock(connString))
                {
                    SQLiteConnection sqlconn = new SQLiteConnection(connString);
                    sqlconn.Open();
                    SQLiteCommand comm = new SQLiteCommand(sqlconn);
                    comm.CommandText = String.Format("UPDATE Label SET XCoord={0},YCoord={1} WHERE LBName='{2}'", XCoord, YCoord, lbname);
                    comm.ExecuteNonQuery();
                    sqlconn.Close();
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*   2017年7月11日 增加成品盤點功能   */
        public static void AddProductPD(string SQL)
        {
            String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            connString = String.Format(ConnString, progPath + "\\pd.db");

            using (SQLiteWriteLock swlock = new SQLiteWriteLock(connString))
            {
                SQLiteConnection conn = new SQLiteConnection(connString);
                conn.Open();
                SQLiteCommand comm = new SQLiteCommand(conn);
                comm.CommandText = SQL;
                comm.ExecuteNonQuery();
                conn.Close();
            }
        }

        // 查询语句，返回数据集
        public static DataSet SelectProductPD(string SQL)
        {
            String progPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            connString = String.Format(ConnString, progPath + "\\pd.db");
            DataSet ds = new DataSet("info");

            using (SQLiteWriteLock swlock = new SQLiteWriteLock(connString))
            {
                SQLiteConnection conn = new SQLiteConnection(connString);
                conn.Open();
                SQLiteCommand comm = new SQLiteCommand(conn);
                comm.CommandText = SQL;
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(comm);
                adapter.Fill(ds);
                conn.Close();
            }

            return ds;
        }

    }

    public class Win32
    {
        public const uint POWER_FORCE = 0x00001000u;
        public const uint POWER_STATE_RESET = 0x00800000u;        // reset state

        [DllImport("coredll.dll")]
        public static extern uint SetSystemPowerState([MarshalAs(UnmanagedType.LPWStr)]string psState, uint StateFlags, uint Options);

        [DllImport("coredll.dll", EntryPoint = "FindWindow")]

        public static extern int FindWindow(string lpWindowName, string lpClassName);
        [DllImport("coredll.dll", EntryPoint = "ShowWindow")]

        public static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);
        [DllImport("coredll.dll", EntryPoint = "SystemParametersInfo")]
        public static extern int SystemParametersInfo(int uAction, int uParam, ref Rectangle lpvParam, int fuWinIni);

        public const int SPI_SETWORKAREA = 47;
        public const int SPI_GETWORKAREA = 48;

        public const int SW_HIDE = 0x00;
        public const int SW_SHOW = 0x0001;
        public const int SPIF_UPDATEINIFILE = 0x01;
    }
}